
package com.c2w.InfoButton;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

public class ImageExample extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        // Load the image
        Image image = new Image(getClass().getResourceAsStream("colorkit.jpg")); // Adjust path as necessary

        // Create an ImageView to display the image
        ImageView imageView = new ImageView(image);

        // Create a StackPane as the root node
        StackPane root = new StackPane();
        root.getChildren().add(imageView);

        // Create the scene
        Scene scene = new Scene(root, 400, 300);

        // Set the stage title and scene, then show the stage
        primaryStage.setTitle("Imageiew Example");
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
