package com.c2w.InfoButton;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;

public class InfoButton extends Application{

    
    
        @Override
        public void start(Stage primaryStage) {
            // Create a GridPane to hold the form elements
            GridPane grid = new GridPane();
            grid.setAlignment(Pos.CENTER);
            grid.setHgap(10);
            grid.setVgap(10);
            grid.setPadding(new Insets(25, 25, 25, 25));
    
            // Add labels and input controls
            Label classNameLabel = new Label("Class Name:");
            ComboBox<String> classNameCombo = new ComboBox<>();
            classNameCombo.getItems().addAll("Class A", "Class B", "Class C"); // Add class names
            grid.add(classNameLabel, 0, 0);
            grid.add(classNameCombo, 1, 0);
    
            Label batchLabel = new Label("Batch:");
            ComboBox<String> batchCombo = new ComboBox<>();
            batchCombo.getItems().addAll("2023", "2024", "2025"); // Add batch years
            grid.add(batchLabel, 0, 1);
            grid.add(batchCombo, 1, 1);
    
            Label subjectLabel = new Label("Subject:");
            TextField subjectField = new TextField();
            grid.add(subjectLabel, 0, 2);
            grid.add(subjectField, 1, 2);
    
            Label teacherNameLabel = new Label("Teacher Name:");
            TextField teacherNameField = new TextField();
            grid.add(teacherNameLabel, 0, 3);
            grid.add(teacherNameField, 1, 3);
    
            // Create the scene and show the stage
            Scene scene = new Scene(grid, 300, 200);
            primaryStage.setTitle("About Class");
            primaryStage.setScene(scene);
            primaryStage.show();
        }
    
       
    }

