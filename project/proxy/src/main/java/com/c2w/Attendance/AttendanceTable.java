package com.c2w.Attendance;

import com.c2w.AllScreens.FirebaseConfig;
import com.c2w.AllScreens.Screen6;



import com.google.api.core.ApiFuture;
import com.google.cloud.firestore.*;
import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.layout.*;
import javafx.stage.Stage;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class AttendanceTable {

    private Stage primaryStage;
    private Scene scene;
    private ObservableList<Person> data;

    public AttendanceTable(Stage primaryStage) {
        this.primaryStage = primaryStage;
        this.data = FXCollections.observableArrayList();
        this.scene = initAttendance(primaryStage);
        loadExistingAttendance();
    }

    public Scene initAttendance(Stage primaryStage) {
        // Create columns for the TableView
        TableColumn<Person, Integer> serialNumberColumn = new TableColumn<>("Sr. No");
        serialNumberColumn.setCellValueFactory(new PropertyValueFactory<>("serialNumber"));
        serialNumberColumn.setPrefWidth(80);

        TableColumn<Person, Integer> rollNumberColumn = new TableColumn<>("Roll No");
        rollNumberColumn.setCellValueFactory(new PropertyValueFactory<>("rollNumber"));
        rollNumberColumn.setPrefWidth(80);

        TableColumn<Person, String> firstNameColumn = new TableColumn<>("First Name");
        firstNameColumn.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        firstNameColumn.setPrefWidth(100);

        TableColumn<Person, String> lastNameColumn = new TableColumn<>("Last Name");
        lastNameColumn.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        lastNameColumn.setPrefWidth(100);

        TableColumn<Person, LocalDate> dateColumn = new TableColumn<>("Date");
        dateColumn.setCellValueFactory(new PropertyValueFactory<>("date"));
        dateColumn.setPrefWidth(100);

        TableColumn<Person, String> attendanceStatusColumn = new TableColumn<>("Attendance");
        attendanceStatusColumn.setCellValueFactory(cellData -> {
            boolean isPresent = cellData.getValue().isPresent();
            return new SimpleStringProperty(isPresent ? "Present" : "Absent");
        });
        attendanceStatusColumn.setPrefWidth(100);


        // Create the TableView and set the data and columns
        TableView<Person> tableView = new TableView<>(data);
        tableView.getColumns().addAll(serialNumberColumn, rollNumberColumn, firstNameColumn, lastNameColumn, dateColumn, attendanceStatusColumn);

        // Set background color for the TableView
        tableView.setStyle("-fx-background-color: lightgray;");

        // Set background color for the rows and cells
        tableView.setRowFactory(tv -> new TableRow<Person>() {
            @Override
            protected void updateItem(Person item, boolean empty) {
                super.updateItem(item, empty);
                if (item == null || empty) {
                    setStyle("");
                } else {
                    if (getIndex() % 2 == 0) {
                        setStyle("-fx-background-color: lightblue;");
                    } else {
                        setStyle("-fx-background-color: lightgreen;");
                    }
                }
            }
        });

        // Create input fields and button for adding new data
        TextField serialNoInput = new TextField();
        serialNoInput.setPromptText("Sr. No");

        TextField rollNoInput = new TextField();
        rollNoInput.setPromptText("Roll No");

        TextField firstNameInput = new TextField();
        firstNameInput.setPromptText("First Name");

        TextField lastNameInput = new TextField();
        lastNameInput.setPromptText("Last Name");

        DatePicker datePicker = new DatePicker();
        datePicker.setPromptText("Select Date");

        ToggleGroup attendanceToggleGroup = new ToggleGroup();
        RadioButton presentInput = new RadioButton("Present");
        RadioButton absentInput = new RadioButton("Absent");
        presentInput.setToggleGroup(attendanceToggleGroup);
        absentInput.setToggleGroup(attendanceToggleGroup);

        Button JI_back = new Button("Back");
        JI_back.setAlignment(Pos.BOTTOM_LEFT);
        JI_back.setOnAction(event ->{
            Screen6 screen6=new Screen6(this, primaryStage);
            primaryStage.setScene(screen6.getScreen6Scene());
            primaryStage.show();


        });
        


        Button addButton = new Button("Add");
        addButton.setOnAction((ActionEvent e) -> {
            try {
                int serialNumber = Integer.parseInt(serialNoInput.getText());
                int rollNumber = Integer.parseInt(rollNoInput.getText());
                String firstName = firstNameInput.getText();
                String lastName = lastNameInput.getText();
                LocalDate date = datePicker.getValue();
                boolean isPresent = presentInput.isSelected();

                Person person = new Person(serialNumber, rollNumber, firstName, lastName, date, isPresent);
                data.add(person);

                serialNoInput.clear();
                rollNoInput.clear();
                firstNameInput.clear();
                lastNameInput.clear();
                datePicker.setValue(null);
                attendanceToggleGroup.selectToggle(null); // Clear radio button selection

                // Update attendance in Firestore
                updateAttendanceInFirestore(person);

            } catch (NumberFormatException ex) {
                System.out.println("Invalid input. Please enter valid numbers for Serial Number and Roll Number.");
            }
        });

        // Layout for input fields and button
        HBox inputLayout = new HBox(10);
        inputLayout.getChildren().addAll(
                new Label("Sr. No:"), serialNoInput,
                new Label("Roll No:"), rollNoInput,
                new Label("First Name:"), firstNameInput,
                new Label("Last Name:"), lastNameInput,
                new Label("Date:"), datePicker,
                presentInput,
                absentInput,
                addButton);

        VBox layout = new VBox(10);
        layout.getChildren().addAll(tableView, inputLayout,JI_back);

        // Load the background image
        Image backgroundImage = new Image("/images/bk1.png");
        BackgroundImage background = new BackgroundImage(
                backgroundImage,
                BackgroundRepeat.NO_REPEAT,
                BackgroundRepeat.NO_REPEAT,
                BackgroundPosition.DEFAULT,
                new BackgroundSize(BackgroundSize.AUTO, BackgroundSize.AUTO, false, false, true, true)
        );

        layout.setBackground(new Background(background));

        // Create the scene and set it on the stage
        Scene scene = new Scene(layout, 900, 500);
        primaryStage.setFullScreen(true);
        return scene;
    }

    public Scene getScene() {
        return scene;
    }

    private void updateAttendanceInFirestore(Person person) {
        Firestore db = FirebaseConfig.db;
        ExecutorService executor = Executors.newSingleThreadExecutor();

        executor.submit(() -> {
            try {
                String date = person.getDate().toString();
                DocumentReference docRef = db.collection("attendance").document(date);
                ApiFuture<DocumentSnapshot> future = docRef.get();
                DocumentSnapshot document = future.get();

                if (document.exists()) {
                    // Update the existing document
                    Map<String, Object> attendance = document.getData();
                    if (attendance != null) {
                        Map<String, Object> studentAttendance = new HashMap<>();
                        studentAttendance.put("firstName", person.getFirstName());
                        studentAttendance.put("lastName", person.getLastName());
                        studentAttendance.put("present", person.isPresent());

                        attendance.put(String.valueOf(person.getRollNumber()), studentAttendance);
                        ApiFuture<WriteResult> writeResult = docRef.update(attendance);
                        writeResult.get(); // Ensure the update completes
                        Platform.runLater(() -> System.out.println("Attendance updated successfully for roll number: " + person.getRollNumber()));
                    }
                } else {
                    // Create a new document
                    Map<String, Object> attendance = new HashMap<>();
                    Map<String, Object> studentAttendance = new HashMap<>();
                    studentAttendance.put("firstName", person.getFirstName());
                    studentAttendance.put("lastName", person.getLastName());
                    studentAttendance.put("present", person.isPresent());

                    attendance.put(String.valueOf(person.getRollNumber()), studentAttendance);
                    ApiFuture<WriteResult> writeResult = docRef.set(attendance);
                    writeResult.get(); // Ensure the set operation completes
                    Platform.runLater(() -> System.out.println("Attendance created successfully for roll number: " + person.getRollNumber()));
                }
            } catch (Exception e) {
                Platform.runLater(() -> System.out.println("Error updating/creating attendance: " + e.getMessage()));
            } finally {
                executor.shutdown();
            }
        });
    }

    private void loadExistingAttendance() {
        Firestore db = FirebaseConfig.db;
        ExecutorService executor = Executors.newSingleThreadExecutor();

        executor.submit(() -> {
            try {
                ApiFuture<QuerySnapshot> future = db.collection("attendance").get();
                QuerySnapshot querySnapshot = future.get();

                for (DocumentSnapshot document : querySnapshot.getDocuments()) {
                    Map<String, Object> attendance = document.getData();
                    if (attendance != null) {
                        for (Map.Entry<String, Object> entry : attendance.entrySet()) {
                            if (entry.getValue() instanceof Map) {
                                Map<String, Object> studentAttendance = (Map<String, Object>) entry.getValue();
                                int rollNumber = Integer.parseInt(entry.getKey());
                                String firstName = (String) studentAttendance.get("firstName");
                                String lastName = (String) studentAttendance.get("lastName");
                                boolean isPresent = (boolean) studentAttendance.get("present");
                                LocalDate date = LocalDate.parse(document.getId());

                                Person person = new Person(data.size() + 1, rollNumber, firstName, lastName, date, isPresent);
                                Platform.runLater(() -> data.add(person));
                            }
                        }
                    }
                }
                Platform.runLater(() -> System.out.println("Existing attendance loaded successfully."));
            } catch (Exception e) {
                Platform.runLater(() -> System.out.println("Error loading existing attendance: " + e.getMessage()));
            } finally {
                executor.shutdown();
            }
        });
    }
    

    public static class Person {
        private final int serialNumber;
        private final int rollNumber;
        private final String firstName;
        private final String lastName;
        private final LocalDate date;
        private final boolean isPresent;

        public Person(int serialNumber, int rollNumber, String firstName, String lastName, LocalDate date, boolean isPresent) {
            this.serialNumber = serialNumber;
            this.rollNumber = rollNumber;
            this.firstName = firstName;
            this.lastName = lastName;
            this.date = date;
            this.isPresent = isPresent;
        }

        public int getSerialNumber() {
            return serialNumber;
        }

        public int getRollNumber() {
            return rollNumber;
        }

        public String getFirstName() {
            return firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public LocalDate getDate() {
            return date;
        }

        public boolean isPresent() {
            return isPresent;
        }
    }
}


// import com.google.api.core.ApiFuture;
// import com.google.cloud.firestore.*;
// import javafx.application.Platform;
// import javafx.collections.FXCollections;
// import javafx.collections.ObservableList;
// import javafx.event.ActionEvent;
// import javafx.scene.Scene;
// import javafx.scene.control.*;
// import javafx.scene.control.cell.PropertyValueFactory;
// import javafx.scene.image.Image;
// import javafx.scene.layout.*;
// import javafx.stage.Stage;

// import java.time.LocalDate;
// import java.util.HashMap;
// import java.util.Map;
// import java.util.concurrent.ExecutorService;
// import java.util.concurrent.Executors;

// public class AttendanceTable {

//     private Stage primaryStage;
//     private Scene scene;
//     private ObservableList<Person> data;

//     public AttendanceTable(Stage primaryStage) {
//         this.primaryStage = primaryStage;
//         this.data = FXCollections.observableArrayList();
//         this.scene = initAttendance(primaryStage);
//         loadExistingAttendance();
//     }

//     public Scene initAttendance(Stage primaryStage) {
//         // Create columns for the TableView
//         TableColumn<Person, Integer> serialNumberColumn = new TableColumn<>("Sr. No");
//         serialNumberColumn.setCellValueFactory(new PropertyValueFactory<>("serialNumber"));
//         serialNumberColumn.setPrefWidth(80);

//         TableColumn<Person, Integer> rollNumberColumn = new TableColumn<>("Roll No");
//         rollNumberColumn.setCellValueFactory(new PropertyValueFactory<>("rollNumber"));
//         rollNumberColumn.setPrefWidth(80);

//         TableColumn<Person, String> firstNameColumn = new TableColumn<>("First Name");
//         firstNameColumn.setCellValueFactory(new PropertyValueFactory<>("firstName"));
//         firstNameColumn.setPrefWidth(100);

//         TableColumn<Person, String> lastNameColumn = new TableColumn<>("Last Name");
//         lastNameColumn.setCellValueFactory(new PropertyValueFactory<>("lastName"));
//         lastNameColumn.setPrefWidth(100);

//         TableColumn<Person, LocalDate> dateColumn = new TableColumn<>("Date");
//         dateColumn.setCellValueFactory(new PropertyValueFactory<>("date"));
//         dateColumn.setPrefWidth(100);

//         TableColumn<Person, Boolean> presentColumn = new TableColumn<>("Present");
//         presentColumn.setCellValueFactory(new PropertyValueFactory<>("present"));
//         presentColumn.setPrefWidth(80);

//         TableColumn<Person, Boolean> absentColumn = new TableColumn<>("Absent");
//         absentColumn.setCellValueFactory(new PropertyValueFactory<>("absent"));
//         absentColumn.setPrefWidth(80);

//         // Create the TableView and set the data and columns
//         TableView<Person> tableView = new TableView<>(data);
//         tableView.getColumns().addAll(serialNumberColumn, rollNumberColumn, firstNameColumn, lastNameColumn, dateColumn, presentColumn, absentColumn);

//         // Set background color for the TableView
//         tableView.setStyle("-fx-background-color: lightgray;");

//         // Set background color for the rows and cells
//         tableView.setRowFactory(tv -> new TableRow<Person>() {
//             @Override
//             protected void updateItem(Person item, boolean empty) {
//                 super.updateItem(item, empty);
//                 if (item == null || empty) {
//                     setStyle("");
//                 } else {
//                     if (getIndex() % 2 == 0) {
//                         setStyle("-fx-background-color: lightblue;");
//                     } else {
//                         setStyle("-fx-background-color: lightgreen;");
//                     }
//                 }
//             }
//         });

//         // Create input fields and button for adding new data
//         TextField serialNoInput = new TextField();
//         serialNoInput.setPromptText("Sr. No");

//         TextField rollNoInput = new TextField();
//         rollNoInput.setPromptText("Roll No");

//         TextField firstNameInput = new TextField();
//         firstNameInput.setPromptText("First Name");

//         TextField lastNameInput = new TextField();
//         lastNameInput.setPromptText("Last Name");

//         DatePicker datePicker = new DatePicker();
//         datePicker.setPromptText("Select Date");

//         CheckBox presentInput = new CheckBox("Present");
//         CheckBox absentInput = new CheckBox("Absent");

//         Button addButton = new Button("Add");
//         addButton.setOnAction((ActionEvent e) -> {
//             try {
//                 int serialNumber = Integer.parseInt(serialNoInput.getText());
//                 int rollNumber = Integer.parseInt(rollNoInput.getText());
//                 String firstName = firstNameInput.getText();
//                 String lastName = lastNameInput.getText();
//                 LocalDate date = datePicker.getValue();
//                 boolean present = presentInput.isSelected();
//                 boolean absent = absentInput.isSelected();

//                 Person person = new Person(serialNumber, rollNumber, firstName, lastName, date, present, absent);
//                 data.add(person);

//                 serialNoInput.clear();
//                 rollNoInput.clear();
//                 firstNameInput.clear();
//                 lastNameInput.clear();
//                 datePicker.setValue(null);
//                 presentInput.setSelected(false);
//                 absentInput.setSelected(false);

//                 // Update attendance in Firestore
//                 updateAttendanceInFirestore(person);

//             } catch (NumberFormatException ex) {
//                 System.out.println("Invalid input. Please enter valid numbers for Serial Number and Roll Number.");
//             }
//         });

//         // Layout for input fields and button
//         HBox inputLayout = new HBox(10);
//         inputLayout.getChildren().addAll(
//                 new Label("Sr. No:"), serialNoInput,
//                 new Label("Roll No:"), rollNoInput,
//                 new Label("First Name:"), firstNameInput,
//                 new Label("Last Name:"), lastNameInput,
//                 new Label("Date:"), datePicker,
//                 presentInput,
//                 absentInput,
//                 addButton);

//         VBox layout = new VBox(10);
//         layout.getChildren().addAll(tableView, inputLayout);

//         // Load the background image
//         Image backgroundImage = new Image("/images/bk1.png");
//         BackgroundImage background = new BackgroundImage(
//                 backgroundImage,
//                 BackgroundRepeat.NO_REPEAT,
//                 BackgroundRepeat.NO_REPEAT,
//                 BackgroundPosition.DEFAULT,
//                 new BackgroundSize(BackgroundSize.AUTO, BackgroundSize.AUTO, false, false, true, true)
//         );

//         layout.setBackground(new Background(background));

//         // Create the scene and set it on the stage
//         Scene scene = new Scene(layout, 900, 500);
//         return scene;
//     }

//     public Scene getScene() {
//         return scene;
//     }

//     private void updateAttendanceInFirestore(Person person) {
//         Firestore db = FirebaseConfig.db;
//         ExecutorService executor = Executors.newSingleThreadExecutor();

//         executor.submit(() -> {
//             try {
//                 String date = person.getDate().toString();
//                 DocumentReference docRef = db.collection("attendance").document(date);
//                 ApiFuture<DocumentSnapshot> future = docRef.get();
//                 DocumentSnapshot document = future.get();

//                 if (document.exists()) {
//                     // Update the existing document
//                     Map<String, Object> attendance = document.getData();
//                     if (attendance != null) {
//                         Map<String, Object> studentAttendance = new HashMap<>();
//                         studentAttendance.put("firstName", person.getFirstName());
//                         studentAttendance.put("lastName", person.getLastName());
//                         studentAttendance.put("present", person.isPresent());
//                         studentAttendance.put("absent", person.isAbsent());

//                         attendance.put(String.valueOf(person.getRollNumber()), studentAttendance);
//                         ApiFuture<WriteResult> writeResult = docRef.update(attendance);
//                         writeResult.get(); // Ensure the update completes
//                         Platform.runLater(() -> System.out.println("Attendance updated successfully for roll number: " + person.getRollNumber()));
//                     }
//                 } else {
//                     // Create a new document
//                     Map<String, Object> attendance = new HashMap<>();
//                     Map<String, Object> studentAttendance = new HashMap<>();
//                     studentAttendance.put("firstName", person.getFirstName());
//                     studentAttendance.put("lastName", person.getLastName());
//                     studentAttendance.put("present", person.isPresent());
//                     studentAttendance.put("absent", person.isAbsent());

//                     attendance.put(String.valueOf(person.getRollNumber()), studentAttendance);
//                     ApiFuture<WriteResult> writeResult = docRef.set(attendance);
//                     writeResult.get(); // Ensure the set operation completes
//                     Platform.runLater(() -> System.out.println("Attendance created successfully for roll number: " + person.getRollNumber()));
//                 }
//             } catch (Exception e) {
//                 Platform.runLater(() -> System.out.println("Error updating/creating attendance: " + e.getMessage()));
//             } finally {
//                 executor.shutdown();
//             }
//         });
//     }

//     private void loadExistingAttendance() {
//         Firestore db = FirebaseConfig.db;
//         ExecutorService executor = Executors.newSingleThreadExecutor();

//         executor.submit(() -> {
//             try {
//                 ApiFuture<QuerySnapshot> future = db.collection("attendance").get();
//                 QuerySnapshot querySnapshot = future.get();

//                 for (DocumentSnapshot document : querySnapshot.getDocuments()) {
//                     Map<String, Object> attendance = document.getData();
//                     if (attendance != null) {
//                         for (Map.Entry<String, Object> entry : attendance.entrySet()) {
//                             if (entry.getValue() instanceof Map) {
//                                 Map<String, Object> studentAttendance = (Map<String, Object>) entry.getValue();
//                                 int rollNumber = Integer.parseInt(entry.getKey());
//                                 String firstName = (String) studentAttendance.get("firstName");
//                                 String lastName = (String) studentAttendance.get("lastName");
//                                 boolean present = (boolean) studentAttendance.get("present");
//                                 boolean absent = (boolean) studentAttendance.get("absent");
//                                 LocalDate date = LocalDate.parse(document.getId());

//                                 Person person = new Person(data.size() + 1, rollNumber, firstName, lastName, date, present, absent);
//                                 Platform.runLater(() -> data.add(person));
//                             }
//                         }
//                     }
//                 }
//                 Platform.runLater(() -> System.out.println("Existing attendance loaded successfully."));
//             } catch (Exception e) {
//                 Platform.runLater(() -> System.out.println("Error loading existing attendance: " + e.getMessage()));
//             } finally {
//                 executor.shutdown();
//             }
//         });
//     }

//     public static class Person {
//         private final int serialNumber;
//         private final int rollNumber;
//         private final String firstName;
//         private final String lastName;
//         private final LocalDate date;
//         private final boolean present;
//         private final boolean absent;

//         public Person(int serialNumber, int rollNumber, String firstName, String lastName, LocalDate date, boolean present, boolean absent) {
//             this.serialNumber = serialNumber;
//             this.rollNumber = rollNumber;
//             this.firstName = firstName;
//             this.lastName = lastName;
//             this.date = date;
//             this.present = present;
//             this.absent = absent;
//         }

//         public int getSerialNumber() {
//             return serialNumber;
//         }

//         public int getRollNumber() {
//             return rollNumber;
//         }

//         public String getFirstName() {
//             return firstName;
//         }

//         public String getLastName() {
//             return lastName;
//         }

//         public LocalDate getDate() {
//             return date;
//         }

//         public boolean isPresent() {
//             return present;
//         }

//         public boolean isAbsent() {
//             return absent;
//         }
//     }
// }
