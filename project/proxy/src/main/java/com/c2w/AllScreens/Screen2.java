package com.c2w.AllScreens;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;

public class Screen2 {
    private Screen1 screen1;
    private Stage sc1Stage;
    private static Scene curScene1;

    public Screen2(Screen1 screen1, Stage sc1Stage){
        this.screen1 = screen1;
        this.sc1Stage = sc1Stage;

        this.curScene1 = initialize(sc1Stage);
    }

    public Scene getScreen2Scene(){
        return curScene1;
    }

    public Scene initialize(Stage prStage2) {
        prStage2.setTitle("Screen2");
        prStage2.setWidth(800);
        prStage2.setHeight(800);
        prStage2.setFullScreen(true);

        // Button "Create Class"
        Button centerBottomButton = new Button("Create Class");
        centerBottomButton.setPrefWidth(300);
        centerBottomButton.setPrefHeight(80);
        Font customFont = Font.font("Courier New", FontWeight.BOLD, 20);
        centerBottomButton.setFont(customFont);

        centerBottomButton.setStyle(
            "-fx-background-color: rgba(230, 230, 250, 0.8); " +
            "-fx-background-radius: 30; " + // Rounded corners
            "-fx-border-radius: 30; " + // Border radius
            "-fx-border-color: black; " + // Optional border color
            "-fx-border-width: 2;" // Optional border width
        );

        centerBottomButton.setOnAction(event -> {
            System.out.println("Create Class button clicked");
            Screen3 screen3 = new Screen3(this, sc1Stage);
            prStage2.setScene(screen3.getScreen3Scene());
            prStage2.show();
        });

        // Button "Join Class"
        Button joinClassButton = new Button("Show Existing  Class");
        joinClassButton.setPrefWidth(300);
        joinClassButton.setPrefHeight(80);
        joinClassButton.setFont(customFont);

        joinClassButton.setStyle(
            "-fx-background-color: rgba(230, 230, 250, 0.8); " +
            "-fx-background-radius: 30; " + // Rounded corners
            "-fx-border-radius: 30; " + // Border radius
            "-fx-border-color: black; " + // Optional border color
            "-fx-border-width: 2;" // Optional border width
        );

        joinClassButton.setOnAction(event -> {
            System.out.println("Exisitng classes");
            Screen3 screen3 = new Screen3(this, sc1Stage);
            Screen4 screen4 = new Screen4(screen3, prStage2);
        
            prStage2.setScene(screen4.getScreen4Scene());
            prStage2.show();
        });

        // Button "Back"
        Button JI_back = new Button("Back");
        Font customFont1 = Font.font("Courier New", FontWeight.BOLD, 20);
        JI_back.setFont(customFont1);
        JI_back.setStyle(
            "-fx-background-color: rgba(230, 230, 250, 0.8); " +
            "-fx-background-radius: 30; " + // Rounded corners
            "-fx-border-radius: 30; " + // Border radius
            "-fx-border-color: black; " + // Optional border color
            "-fx-border-width: 2;" // Optional border width
        );
        VBox topLeftLayout = new VBox(20, JI_back); // 20 is the spacing between buttons
        topLeftLayout.setAlignment(Pos.CENTER);

        JI_back.setOnAction(event -> {
            System.out.println("Back button clicked");
            sc1Stage.setScene(screen1.getScreen1Scene());
            System.out.println("Back");
        });

        // Background image
        Image bkimg = new Image("/images/bk1.png");
        ImageView bkimgview = new ImageView(bkimg);
        bkimgview.setPreserveRatio(true);
        bkimgview.setFitWidth(3000);
        bkimgview.setFitHeight(1200);
        bkimgview.setMouseTransparent(true); // This line makes the image view not intercept mouse events

        // Layout for buttons
       

        VBox centerLayout = new VBox(20,centerBottomButton, joinClassButton,topLeftLayout); // 20 is the spacing between buttons
        centerLayout.setAlignment(Pos.CENTER);

        // StackPane for positioning
        StackPane stackPane = new StackPane();
        stackPane.getChildren().addAll(bkimgview,  centerLayout);

        BorderPane mainLayout = new BorderPane();
        mainLayout.setCenter(stackPane);
        BorderPane.setAlignment(topLeftLayout, Pos.TOP_LEFT);
        BorderPane.setAlignment(centerLayout, Pos.CENTER);

        // Main layout scene
        Scene sc = new Scene(mainLayout, 600, 400);
        return sc;
    }
}
