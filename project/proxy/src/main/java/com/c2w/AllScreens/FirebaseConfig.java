package com.c2w.AllScreens;

import com.google.api.core.ApiFuture;
import com.google.api.gax.core.FixedExecutorProvider;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.firestore.*;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.cloud.FirestoreClient;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

public class FirebaseConfig {

    public static Firestore db;

    static {
        try {
            Firebaseinit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static List<String> getImageUrls(String documentId) {
        Firestore db = getFirestore();
        DocumentReference docRef = db.collection("classes").document(documentId);

        System.out.println("1111");

        try {
            DocumentSnapshot document = docRef.get().get();
            if (document.exists()) {
                List<String> imageUrls = (List<String>) document.get("imageUrls");
                return imageUrls != null ? imageUrls : new ArrayList<>();
            } else {
                System.out.println("No such document!");
                return new ArrayList<>();
            }
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
            return new ArrayList<>();
        }
    }


    public static void Firebaseinit() {
        if (FirebaseApp.getApps().isEmpty()) {  // Check if FirebaseApp is already initialized
            try {
                FileInputStream serviceAccount = new FileInputStream("src/main/resources/proxy.json");

                FirebaseOptions options = new FirebaseOptions.Builder()
                        .setCredentials(GoogleCredentials.fromStream(serviceAccount))
                        // .setExecutorProvider(FixedExecutorProvider.create(Executors.newFixedThreadPool(4))) // Example executor
                        .build();

                FirebaseApp.initializeApp(options);
                db = FirestoreClient.getFirestore();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        
    }
    

    public static Firestore getFirestore() {
        return db;
    }

    public static void updateDocumentByTheDocumentIdWithImageUrlArray(String url, String id) {
        Firestore db = FirebaseConfig.getFirestore();
        DocumentReference docRef = db.collection("classes").document(id);

        ApiFuture<WriteResult> future = docRef.update("imageUrls", FieldValue.arrayUnion(url));

        try {
            WriteResult result = future.get();
            System.out.println("Update time : " + result.getUpdateTime());
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
    }

    public static ApiFuture<WriteResult> writeDocument(String collection, Object data) throws Exception {
        Long newId = getNextAutoIncrementId(collection);
        DocumentReference docRef = db.collection(collection).document(String.valueOf(newId));
        return docRef.set(data);
    }

    private static Long getNextAutoIncrementId(String collection) throws Exception {
        DocumentReference counterDocRef = db.collection("counters").document(collection + "Counter");

        ApiFuture<DocumentSnapshot> futureSnapshot = counterDocRef.get();
        DocumentSnapshot document = futureSnapshot.get();
        if (!document.exists()) {
            Map<String, Object> initialData = new HashMap<>();
            initialData.put("currentValue", 0L);
            counterDocRef.set(initialData).get();
        }

        ApiFuture<Long> future = db.runTransaction(transaction -> {
            DocumentSnapshot snapshot = transaction.get(counterDocRef).get();
            long currentValue = snapshot.getLong("currentValue");
            long newValue = currentValue + 1;
            transaction.update(counterDocRef, "currentValue", newValue);
            return newValue;
        });

        return future.get();
    }

    public static ApiFuture<DocumentSnapshot> readDocument(String collection, String documentId) {
        DocumentReference docRef = db.collection(collection).document(documentId);
        return docRef.get();
    }

    public static ApiFuture<QuerySnapshot> readCollection(String collection) {
        CollectionReference colRef = db.collection(collection);
        return colRef.get();
    }

   



public static List<String> getStudentClasses(String studentId) {
    Firestore db = getFirestore();
    CollectionReference classesCollection = db.collection("classes");

    ApiFuture<QuerySnapshot> future = classesCollection.get();

    try {
        QuerySnapshot querySnapshot = future.get();
        List<String> classCodes = new ArrayList<>();

        for (QueryDocumentSnapshot document : querySnapshot) {
            List<String> students = (List<String>) document.get("students");
            if (students != null && students.contains(studentId)) {
                classCodes.add(document.getString("batch"));
            }
        }

        return classCodes;  // Return the list of class codes

    } catch (InterruptedException | ExecutionException e) {
        e.printStackTrace();
        return null;
    }
}



    public static void addStudentToClass(String classCode, String studentId) {
        Firestore db = FirebaseConfig.getFirestore();
        
        Query query = db.collection("classes").whereEqualTo("code", classCode);
        ApiFuture<QuerySnapshot> querySnapshot = query.get();

        try {
            List<QueryDocumentSnapshot> documents = querySnapshot.get().getDocuments();
            if (!documents.isEmpty()) {
                DocumentReference classDocRef = documents.get(0).getReference();

                ApiFuture<WriteResult> future = classDocRef.update("students", FieldValue.arrayUnion(studentId));
                WriteResult result = future.get();
                System.out.println("Update time : " + result.getUpdateTime());
            } else {
                System.out.println("No class found with the given code.");
            }
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
    }

    public static ApiFuture<WriteResult> signUpStudent(String username, String email, String password) throws Exception {
        Map<String, Object> studentData = new HashMap<>();
        studentData.put("username", username);
        studentData.put("email", email);
        studentData.put("password", password);
        
        return writeDocument("students", username, studentData);
    }

    public static ApiFuture<WriteResult> signUpFaculty(String username, String email, String password) throws Exception {
        Map<String, Object> facultyData = new HashMap<>();
        facultyData.put("username", username);
        facultyData.put("email", email);
        facultyData.put("password", password);
        
        return writeDocument("faculty", username, facultyData);
    }

    public static ApiFuture<WriteResult> writeDocument(String collection, String documentId, Object data) {
        DocumentReference docRef = db.collection(collection).document(documentId);
        return docRef.set(data);
    }


    public static boolean authenticateUser(String collection, String username, String password) {
        // Ensure Firestore is initialized
        // Firestore firestore = Firebaseinit();
        System.out.println(db);
        CollectionReference users = db.collection(collection);

        try {
            System.out.println("Attempting authentication for user: " + username);
            System.out.println("Password: " + password);

            ApiFuture<QuerySnapshot> future = users.whereEqualTo("username", username)
                                                  .whereEqualTo("password", password)
                                                  .get();
            QuerySnapshot querySnapshot = future.get();

            if (!querySnapshot.isEmpty()) {
                System.out.println("Authentication successful for user: " + username);
                return true;
            } else {
                System.out.println("Authentication failed for user: " + username);
                return false;
            }
        } catch (Exception e) {
            System.err.println("Error during authentication: " + e.getMessage());
            e.printStackTrace();
            return false;
        }
    }
}
