package com.c2w.AllScreens;

public class Demo {
    
}
package com.c2w.view;

import java.util.concurrent.ExecutionException;

import org.omg.CORBA.PUBLIC_MEMBER;

import com.c2w.configuratrion.FirebaseInitialization;
import com.c2w.model.Player;
import com.google.api.core.ApiFuture;
import com.google.cloud.firestore.Blob;
import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.DocumentSnapshot;
import com.google.cloud.firestore.Firestore;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;

import javafx.scene.layout.VBox;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;

public class PlayerInfoPage {
    private static Player player; // URL of the playerinformation page
    private VBox vb;
    private static Firestore db;
    //private static Firestore db;
    
    /*
     * Constructor to initialize PlayerInfoPage with a specific
     * URL.
     * 
     * @param z2o_apt_url The URL of the player information page.
     */

    public PlayerInfoPage(Player z2o_apt_url,Runnable z2o_apt_backHandler)  {
        PlayerInfoPage.player = z2o_apt_url;
        createPlayerInfoScene(z2o_apt_backHandler);
    }

/* 
    public customer getData(String collection,String document){

        try {
            System.out.println("Fetching data from collection"+collection+", document ID :"+document);
            DocumentReference docref=db.collection(collection).document(document);
            ApiFuture<DocumentSnapshot> future=docref.get();
            DocumentSnapshot documentSnapshot=future.get();
            System.out.println("DocumentSnapshot received"+ documentSnapshot);
            if(documentSnapshot.exists()){

                System.out.println("Documnet Exist,exracting data");
                Customer customer=new Customer();
                customer.setBattingStyle(documentSnapshot.getString("batting"));
            }else{
                System.out.println("Document not exist");
                return null;
            }
        } catch (Exception e) {
            System.out.println("An eroot occurd");
            e.printStackTrace();
        }
            

    }
        */
    
/* */
        public VBox readRec() throws InterruptedException, ExecutionException {
            VBox vb1= new VBox();
            System.out.println(db);
            DocumentReference docRef = FirebaseInitialization.db.collection("player").document();
            ApiFuture<DocumentSnapshot> snapShot = docRef.get();
            DocumentSnapshot docSnap = snapShot.get();
            if (docSnap.exists()) {
              Label l1 = new Label(docSnap.getString("battingStyle"));
              Label l2 =new Label(docSnap.getString("bowlingStyle"));
              Label l3 =new Label(docSnap.getString("country"));
              Label l4 =new Label(docSnap.getString("playerAge"));
              Label l5 =new Label(docSnap.getString("playerName"));
              Label l6 =new Label(docSnap.getString("playerRole"));
              
        // System.out.println("Man of Match: " + docSnap.getString("manOfMatch"));
                vb1.getChildren().addAll(l1,l2,l3,l4,l5,l6);
            } else {
                System.out.println("Document Not Found");
            }
            return vb1;
        }
    /*
     * Method to create the player information scene.
     * 
     * @param z2o_apt_backHandler Runnable handler for the back
     * button action.
     * 
     * @return The VBox containing the player information UI
     * components.
     */
    public VBox createPlayerInfoScene(Runnable z2o_apt_backHandler)  {
        
        
        System.out.println("DDDDDDDDDDDDDDDDDDDD");
        // Back button setup
        VBox vBox = null;
        try {
            vBox = readRec();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } 
        Button z2o_apt_backButton = new Button("Back");
        z2o_apt_backButton.setStyle("-fx-pref-width:120;-fx-min-height: 30;-fx-background-radius: 15;-fx-background-color : #2196F3; -fx-text-fill:#FFFFFF");

        // HBox to hold the back button
        HBox z2o_apt_header = new HBox(50, z2o_apt_backButton);

        z2o_apt_header.setStyle("-fx-pref-width: 800;-fx-pref-height: 30;");

        // Set action for the back button
        z2o_apt_backButton.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                z2o_apt_backHandler.run(); // Execute the backhandler (logout handler)

            }
        });
       

         Image image = new Image(player.getPlayerImg());
         ImageView imageView = new ImageView(image);

        Label l0 = new Label(player.getPlayerAge());
        Label l1=new Label(player.getPlayerName());
        Label l2 =new Label(player.getPlayerRole());
        Label l3 =new Label(player.getBattingStyle());
        Label l4 =new Label(player.getBowlingStyle());


        HBox z2o_apt_Box0=new HBox(imageView);
        HBox z2o_apt_Box1=new HBox(l1);
        HBox z2o_apt_Box2=new HBox(l2);

        HBox z2o_apt_Box3=new HBox(l3);

        HBox z2o_apt_Box4=new HBox(l4);
        HBox z2o_apt_Box5=new HBox(l0);
        VBox z2o_apt_vbox0=new VBox(z2o_apt_Box1,z2o_apt_Box2,z2o_apt_Box3,z2o_apt_Box4,z2o_apt_Box5);
        z2o_apt_vbox0.setAlignment(Pos.CENTER);

        VBox z2o_apt_vbox1=new VBox(20,z2o_apt_Box0,z2o_apt_vbox0);
        z2o_apt_vbox1.setLayoutX(600);
        z2o_apt_vbox1.setAlignment(Pos.CENTER);


        



        AnchorPane anchorPane=new AnchorPane(z2o_apt_vbox1); 
        

        VBox z2o_apt_vb = new VBox(30,z2o_apt_backButton,anchorPane,vBox);//////////////

        z2o_apt_vb.setStyle("-fx-background-color:#D3D3D3;-fx-padding: 30;"); // Set background color and padding for
                                                                             // theVBox
        this.vb=z2o_apt_vb;
        return vb; // Return the created VBox containingthe player information UI
    }

    public VBox getVBox() {
       return vb;
    }
}