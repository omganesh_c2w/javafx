
package com.c2w.AllScreens;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class SignUp {
    private Stage primaryStage;
    private Scene scene;

    public SignUp(Stage primaryStage) {
        this.primaryStage = primaryStage;
        this.scene = initialize(primaryStage);
    }

    public Scene getScene() {
        return scene;
    }

    public Scene initialize(Stage primaryStage) {
        primaryStage.setTitle("Sign Up");
        primaryStage.setFullScreen(true);

        GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(25, 25, 25, 25));

        Text sceneTitle = new Text("Sign Up");
        sceneTitle.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));
        grid.add(sceneTitle, 0, 0, 2, 1);

        Label userNameLabel = new Label("Username:");
        grid.add(userNameLabel, 0, 1);

        TextField userNameField = new TextField();
        grid.add(userNameField, 1, 1);

        Label emailLabel = new Label("Email:");
        grid.add(emailLabel, 0, 2);

        TextField emailField = new TextField();
        grid.add(emailField, 1, 2);

        Label passwordLabel = new Label("Password:");
        grid.add(passwordLabel, 0, 3);

        PasswordField passwordField = new PasswordField();
        grid.add(passwordField, 1, 3);

        Label confirmPasswordLabel = new Label("Confirm Password:");
        grid.add(confirmPasswordLabel, 0, 4);

        PasswordField confirmPasswordField = new PasswordField();
        grid.add(confirmPasswordField, 1, 4);

        Button studentSignUpButton = new Button("Student Sign Up");
        HBox studentSignUpHBox = new HBox(10);
        studentSignUpHBox.setAlignment(Pos.BOTTOM_LEFT);
        studentSignUpHBox.getChildren().add(studentSignUpButton);
        grid.add(studentSignUpHBox, 1, 5);

        Button facultySignUpButton = new Button("Faculty Sign Up");
        HBox facultySignUpHBox = new HBox(10);
        facultySignUpHBox.setAlignment(Pos.BOTTOM_RIGHT);
        facultySignUpHBox.getChildren().add(facultySignUpButton);
        grid.add(facultySignUpHBox, 1, 6);

        final Text actionTarget = new Text();
        grid.add(actionTarget, 1, 7);

        studentSignUpButton.setOnAction(e -> {
            String username = userNameField.getText();
            String email = emailField.getText();
            String password = passwordField.getText();
            String confirmPassword = confirmPasswordField.getText();

            if (password.equals(confirmPassword)) {
                try {
                    FirebaseConfig.signUpStudent(username, email, password);
                    actionTarget.setFill(Color.GREEN);
                    actionTarget.setText("Student sign-up successful!");
                } catch (Exception ex) {
                    actionTarget.setFill(Color.RED);
                    actionTarget.setText("Sign-up failed: " + ex.getMessage());
                }
            } else {
                actionTarget.setFill(Color.RED);
                actionTarget.setText("Passwords do not match!");
            }
        });

        facultySignUpButton.setOnAction(e -> {
            String username = userNameField.getText();
            String email = emailField.getText();
            String password = passwordField.getText();
            String confirmPassword = confirmPasswordField.getText();

            if (password.equals(confirmPassword)) {
                try {
                    FirebaseConfig.signUpFaculty(username, email, password);
                    actionTarget.setFill(Color.GREEN);
                    actionTarget.setText("Faculty sign-up successful!");
                } catch (Exception ex) {
                    actionTarget.setFill(Color.RED);
                    actionTarget.setText("Sign-up failed: " + ex.getMessage());
                }
            } else {
                actionTarget.setFill(Color.RED);
                actionTarget.setText("Passwords do not match!");
            }
        });

        Scene scene = new Scene(grid, 300, 275);
        return scene;
    }
}



    

