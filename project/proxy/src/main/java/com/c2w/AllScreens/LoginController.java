 package com.c2w.AllScreens;
import com.google.api.core.ApiFuture;
import com.google.cloud.firestore.DocumentSnapshot;
import com.google.cloud.firestore.WriteResult;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;

import java.util.concurrent.ExecutionException;

public class LoginController {

    private LandingPage landingPage;
    private Stage stage;
    private Scene currentScene;
    private Screen5 screen5;
    public static String userName;
     double mainWidth= 1900;
     double mainHeight=900;

    public LoginController(LandingPage landingPage, Stage stage) {
        this.landingPage = landingPage;
        this.stage = stage;
        this.currentScene = initialize(stage);
    }

    public LoginController(Screen5 screen5, Stage stage){
        this.screen5=screen5;
        this.stage=stage;
        this.currentScene = initialize(stage);
    }

    public Scene getLoginScreenScene() {
        return currentScene;
    }

    public Scene initialize(Stage primaryStage) {
        primaryStage.setTitle("Login");

        AnchorPane main = new AnchorPane();

        // Create anchor panes for student, faculty, and admin logins
        AnchorPane studentAnchorPane = createStudentAnchorPane();
        AnchorPane facultyAnchorPane = createFacultyAnchorPane();
        AnchorPane adminAnchorPane = createAdminAnchorPane();

        // Set initial visibility
        studentAnchorPane.setVisible(true);
        facultyAnchorPane.setVisible(false);
        adminAnchorPane.setVisible(false);

        // Create ComboBox for login options
        ComboBox<String> loginOptions = new ComboBox<>();
        loginOptions.getItems().addAll("Student", "Faculty", "Admin");
        loginOptions.setValue("Student"); // Set default value
        loginOptions.setPrefWidth(200);
        loginOptions.setStyle("-fx-position:relative;");

        // Position the ComboBox
        AnchorPane.setLeftAnchor(loginOptions, mainWidth/2+100);
        AnchorPane.setLeftAnchor(loginOptions, mainHeight/2+100);

        AnchorPane.setTopAnchor(loginOptions, 75.0); // Adjusted to be below the login label

        // Handle ComboBox selection changes
        loginOptions.setOnAction(event -> {
            String selectedOption = loginOptions.getValue();
            if (selectedOption.equals("Student")) {
                studentAnchorPane.setVisible(true);
                facultyAnchorPane.setVisible(false);
                adminAnchorPane.setVisible(false);
            } else if (selectedOption.equals("Faculty")) {
                studentAnchorPane.setVisible(false);
                facultyAnchorPane.setVisible(true);
                adminAnchorPane.setVisible(false);
            } else if (selectedOption.equals("Admin")) {
                studentAnchorPane.setVisible(false);
                facultyAnchorPane.setVisible(false);
                adminAnchorPane.setVisible(true);
            }
        });

        main.getChildren().addAll(studentAnchorPane, facultyAnchorPane, adminAnchorPane, loginOptions);

        Image backgroundImage = new Image("images/login.png");
        BackgroundImage backgroundImg = new BackgroundImage(backgroundImage, BackgroundRepeat.NO_REPEAT,
                BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER,
                new BackgroundSize(BackgroundSize.AUTO, BackgroundSize.AUTO, true, true, true, true));

        Background background = new Background(backgroundImg);
        main.setBackground(background);
        

        Scene scene = new Scene(main, mainWidth,mainHeight);
        mainWidth= scene.getWidth();
        mainHeight= scene.getHeight();
        System.out.println("*******");
        System.out.println(scene.getWidth());
        System.out.println("*******");
        return scene;
    }
    VBox blur = new VBox();
    private AnchorPane createStudentAnchorPane() {
        AnchorPane studentAnchorPane = new AnchorPane();
        studentAnchorPane.setPrefSize(400, 350);
        studentAnchorPane.setStyle("-fx-border-radius: 50; -fx-border-radius: 10; -fx-border-width: 2px; -fx-background-color:#FFFFFF; -fx-padding:100px;-fx-margin-left:100px;");
        
        studentAnchorPane.setPrefWidth(500);
        studentAnchorPane.setLayoutX((mainWidth/2+1)-(studentAnchorPane.getPrefWidth()/2-1));

        studentAnchorPane.setPrefHeight(300);
        studentAnchorPane.setLayoutY((mainHeight/2+1)-(studentAnchorPane.getPrefHeight()/2-1));

        
        
        

        Label studentLogin = new Label("Student Login");
        studentLogin.setTextFill(Color.BLUE);
        studentLogin.setStyle("-fx-text-fill: BLACK; -fx-font-weight: BOLD; -fx-font-size: 50;");
        AnchorPane.setLeftAnchor(studentLogin, 50.0);
        AnchorPane.setTopAnchor(studentLogin, 1.0);

        Label usernameLabel = new Label("Username:");
        usernameLabel.setStyle("-fx-text-fill: Black; -fx-font-weight: BOLD; -fx-font-size: 18;");
        AnchorPane.setLeftAnchor(usernameLabel, 100.0);
        AnchorPane.setTopAnchor(usernameLabel, 150.0); // Adjusted for ComboBox

        TextField usernameInput = new TextField();
        usernameInput.setPromptText("Enter your username");
        usernameInput.setStyle("-fx-text-fill: Black; -fx-font-weight: BOLD; -fx-font-size: 18;");
        AnchorPane.setLeftAnchor(usernameInput, 200.0);
        AnchorPane.setTopAnchor(usernameInput, 150.0); // Adjusted for ComboBox

        Label passwordLabel = new Label("Password:");
        passwordLabel.setStyle("-fx-text-fill: Black; -fx-font-weight: BOLD; -fx-font-size: 18;");
        AnchorPane.setLeftAnchor(passwordLabel, 100.0);
        AnchorPane.setTopAnchor(passwordLabel, 250.0); // Adjusted for ComboBox

        PasswordField passwordInput = new PasswordField();
        passwordInput.setPromptText("Enter your password");
        passwordInput.setStyle("-fx-text-fill: Black; -fx-font-weight: BOLD; -fx-font-size: 18;");
        AnchorPane.setLeftAnchor(passwordInput, 200.0);
        AnchorPane.setTopAnchor(passwordInput, 250.0); // Adjusted for ComboBox

        Button loginButton = createLoginButton(usernameInput, passwordInput, "students");
        AnchorPane.setLeftAnchor(loginButton, 200.0);
        AnchorPane.setTopAnchor(loginButton, 350.0); // Adjusted for ComboBox

        Button signUpButton = createSignUpButton();
        AnchorPane.setLeftAnchor(signUpButton, 300.0);
        AnchorPane.setTopAnchor(signUpButton, 350.0); 
        signUpButton.setStyle("-fx-background-color: blue; -fx-text-fill: white; -fx-font-weight: bold; -fx-font-size: 18;");
        signUpButton.setOnAction(event -> {
            System.out.println("11111111111111");
            SignUp signUp = new SignUp(stage);
            stage.setScene(signUp.getScene());
            stage.show();
        });
        
        studentAnchorPane.getChildren().addAll(studentLogin, usernameLabel, usernameInput, passwordLabel, passwordInput, loginButton, signUpButton);

        return studentAnchorPane;
    }
        //faculty login
    private AnchorPane createFacultyAnchorPane() {
        AnchorPane facultyAnchorPane = new AnchorPane();
        facultyAnchorPane.setPrefSize(400, 350);
        facultyAnchorPane.setStyle("-fx-border-radius: 50; -fx-border-radius: 10; -fx-border-width: 2px; -fx-background-color:#FFFFFF; -fx-padding:100px;-fx-margin-left:100px;");

        facultyAnchorPane.setPrefWidth(500);
        facultyAnchorPane.setLayoutX((mainWidth/2+1)-(facultyAnchorPane.getPrefWidth()/2-1));

        facultyAnchorPane.setPrefHeight(300);
        facultyAnchorPane.setLayoutY((mainHeight/2+1)-(facultyAnchorPane.getPrefHeight()/2-1));

        Label facultyLogin = new Label("Faculty Login");
        facultyLogin.setTextFill(Color.BLUE);
        facultyLogin.setStyle("-fx-text-fill: BLACK; -fx-font-weight: BOLD; -fx-font-size: 50;");
        AnchorPane.setLeftAnchor(facultyLogin, 50.0);
        AnchorPane.setTopAnchor(facultyLogin, 1.0);

        Label usernameLabel = new Label("Username:");
        usernameLabel.setStyle("-fx-text-fill: Black; -fx-font-weight: BOLD; -fx-font-size: 18;");
        AnchorPane.setLeftAnchor(usernameLabel, 100.0);
        AnchorPane.setTopAnchor(usernameLabel, 150.0); // Adjusted for ComboBox

        TextField usernameInput = new TextField();
        usernameInput.setPromptText("Enter your username");
        usernameInput.setStyle("-fx-text-fill: Black; -fx-font-weight: BOLD; -fx-font-size: 18;");
        AnchorPane.setLeftAnchor(usernameInput, 200.0);
        AnchorPane.setTopAnchor(usernameInput, 150.0); // Adjusted for ComboBox

        Label passwordLabel = new Label("Password:");
        passwordLabel.setStyle("-fx-text-fill: Black; -fx-font-weight: BOLD; -fx-font-size: 18;");
        AnchorPane.setLeftAnchor(passwordLabel, 100.0);
        AnchorPane.setTopAnchor(passwordLabel, 250.0); // Adjusted for ComboBox

        PasswordField passwordInput = new PasswordField();
        passwordInput.setPromptText("Enter your password");
        passwordInput.setStyle("-fx-text-fill: Black; -fx-font-weight: BOLD; -fx-font-size: 18;");
        AnchorPane.setLeftAnchor(passwordInput, 200.0);
        AnchorPane.setTopAnchor(passwordInput, 250.0); // Adjusted for ComboBox

        Button loginButton = createLoginButton(usernameInput, passwordInput, "faculty");
        AnchorPane.setLeftAnchor(loginButton, 200.0);
        AnchorPane.setTopAnchor(loginButton, 350.0); // Adjusted for ComboBox

        Button signUpButton = createSignUpButton();
        AnchorPane.setLeftAnchor(signUpButton, 300.0);
        AnchorPane.setTopAnchor(signUpButton, 350.0); 
        signUpButton.setStyle("-fx-background-color: blue; -fx-text-fill: white; -fx-font-weight: bold; -fx-font-size: 18;");
        signUpButton.setOnAction(event -> {
            System.out.println("11111111111111");
            SignUp signUp = new SignUp(stage);
            stage.setScene(signUp.getScene());
            stage.show();
        });
        

        facultyAnchorPane.getChildren().addAll(facultyLogin, usernameLabel, usernameInput, passwordLabel, passwordInput, loginButton, signUpButton);

        return facultyAnchorPane;
    }

    private AnchorPane createAdminAnchorPane() {
        AnchorPane adminAnchorPane = new AnchorPane();
        adminAnchorPane.setPrefSize(400, 350);
        adminAnchorPane.setStyle("-fx-border-radius: 50; -fx-border-radius: 10; -fx-border-width: 2px; -fx-background-color:#FFFFFF; -fx-padding:100px;-fx-margin-left:100px;");
        adminAnchorPane.setPrefWidth(500);
        adminAnchorPane.setLayoutX((mainWidth/2+1)-(adminAnchorPane.getPrefWidth()/2-1));

        adminAnchorPane.setPrefHeight(300);
        adminAnchorPane.setLayoutY((mainHeight/2+1)-(adminAnchorPane.getPrefHeight()/2-1));

        Label adminLogin = new Label("Admin Login");
        adminLogin.setTextFill(Color.BLUE);
        adminLogin.setStyle("-fx-text-fill: BLACK; -fx-font-weight: BOLD; -fx-font-size: 50;");
        AnchorPane.setLeftAnchor(adminLogin, 50.0);
        AnchorPane.setTopAnchor(adminLogin, 1.0);

        Label usernameLabel = new Label("Username:");
        usernameLabel.setStyle("-fx-text-fill: Black; -fx-font-weight: BOLD; -fx-font-size: 18;");
        AnchorPane.setLeftAnchor(usernameLabel, 100.0);
        AnchorPane.setTopAnchor(usernameLabel, 150.0); // Adjusted for ComboBox

        TextField usernameInput = new TextField();
        usernameInput.setPromptText("Enter your username");
        usernameInput.setStyle("-fx-text-fill: Black; -fx-font-weight: BOLD; -fx-font-size: 18;");
        AnchorPane.setLeftAnchor(usernameInput, 200.0);
        AnchorPane.setTopAnchor(usernameInput, 150.0); // Adjusted for ComboBox

        Label passwordLabel = new Label("Password:");
        passwordLabel.setStyle("-fx-text-fill: Black; -fx-font-weight: BOLD; -fx-font-size: 18;");
        AnchorPane.setLeftAnchor(passwordLabel, 100.0);
        AnchorPane.setTopAnchor(passwordLabel, 250.0); // Adjusted for ComboBox

        PasswordField passwordInput = new PasswordField();
        passwordInput.setPromptText("Enter your password");
        passwordInput.setStyle("-fx-text-fill: Black; -fx-font-weight: BOLD; -fx-font-size: 18;");
        AnchorPane.setLeftAnchor(passwordInput, 200.0);
        AnchorPane.setTopAnchor(passwordInput, 250.0); // Adjusted for ComboBox

        Button loginButton = createLoginButton(usernameInput, passwordInput, "admin");
        AnchorPane.setLeftAnchor(loginButton, 200.0);
        AnchorPane.setTopAnchor(loginButton, 350.0); // Adjusted for ComboBox

        adminAnchorPane.getChildren().addAll(adminLogin, usernameLabel, usernameInput, passwordLabel, passwordInput, loginButton);

        return adminAnchorPane;
    }

    private Button createLoginButton(TextField usernameInput, PasswordField passwordInput, String userType) {
        Button loginButton = new Button("Login");
        loginButton.setStyle("-fx-background-color: blue; -fx-text-fill: white; -fx-font-weight: bold; -fx-font-size: 18;");
    
        loginButton.setOnAction(event -> {
            String username = usernameInput.getText();
            String password = passwordInput.getText();
    
            try {
                System.out.println(userType);
                System.out.println(username);
                userName=username;
                System.out.println(password);
                // Adjust userType parameter based on your Firestore collection/document structure
                if (FirebaseConfig.authenticateUser(userType, username, password)) {
                    // Authentication successful, navigate to appropriate screen
                    if (userType.equals("faculty")) {
                        System.out.println("2222222");
                        Screen1 screen1 = new Screen1(stage);
                        stage.setScene(screen1.getScreen1Scene());
                        stage.show();
                    } else if (userType.equals("students")) {

                        //Screen1 screen1 = new Screen1(stage);
                        StudentloginScreen1 studentloginScreen1 = new StudentloginScreen1(this,stage);
                        stage.setScene(studentloginScreen1.getScene());
                        stage.show();
                       
                    } else if (userType.equals("admin")) {
                        // Navigate to admin screen
                    }
                    stage.show();
                } else {
                    // Handle authentication failure (e.g., show error message)
                    System.out.println("Authentication failed. Please check your credentials.");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    
        return loginButton;
    }
    

    private Button createSignUpButton() {
        Button signUpButton = new Button("Sign Up");
        signUpButton.setStyle("-fx-background-color: blue; -fx-text-fill: white; -fx-font-weight: bold; -fx-font-size: 18;");

        signUpButton.setOnAction(event -> {
            System.out.println("Sign Up button clicked!");
            // Add logic to handle sign-up action, such as navigating to a sign-up screen
            SignUp signUp = new SignUp(stage);
            stage.setScene(signUp.getScene());
            stage.show();
        });

        return signUpButton;
    }
}
