package com.c2w.AllScreens;

import java.io.File;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Line;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

public class Screen7  {
    private Screen6 screen6;
    private Stage sc1Stage;
    private Scene currScene6;
    private static final String BUCKET_NAME = "gs://proxy-454f6.appspot.com";

   
    
    public Screen7(Screen6 screen6, Stage sc1Stage){
        this.screen6=screen6;
        this.sc1Stage=sc1Stage;

        this.currScene6=initialize(sc1Stage);
    }

    public Scene getScreen7Scene(){
        return currScene6;
    }

    public Scene initialize(Stage JI9_Stage){

        // @Override
    // public void start(Stage JI9_Stage) {
        Font ft = Font.font("Courier New", FontWeight.BOLD, 30);
        JI9_Stage.setTitle("Screen7"); // Title Of Stage
        JI9_Stage.setHeight(500); // Height Of Stage
        JI9_Stage.setWidth(700); // Width Of Stage
        JI9_Stage.setFullScreen(true);

        // JI9_Stage.setResizable(true); // Resizing Stage

        // Create top pane with a line
        Pane JI9_pane = new Pane();
        Line li = new Line();
        li.setStartX(0);
        li.setStartY(70); // Distance from the top of the screen
        li.setEndX(2000); // Length of the line
        li.setEndY(70);
        JI9_pane.getChildren().addAll(li);

        // Image svgLogo = new Image("file:resources/images/bk1.jpg");
        // ImageView svgLogoView = new ImageView(svgLogo);
        // svgLogoView.setFitWidth(20);
        // svgLogoView.setFitHeight(20);

        // Create a label
        Label JI9_label1 = new Label("Upload Study Material");
        JI9_label1.setFont(ft);
        
        // TextField with prompt text
        TextField textField = new TextField();
        textField.setMaxWidth(450);
        textField.setPromptText("Paste Your Link Here");

        // Buttons

        Button JI9_backButton = new Button("Back");
        JI9_backButton.setAlignment(Pos.BOTTOM_LEFT);
        JI9_backButton.setFont(ft);
        JI9_backButton.setStyle(
          "-fx-background-color: rgba(230, 230, 250, 0.8); " +
          "-fx-background-radius: 30; " + // Rounded corners
          "-fx-border-radius: 30; " + // Border radius
          "-fx-border-color: black; " + // Optional border color
          "-fx-border-width: 2;" // Optional border width
      );
        DropShadow JI_shadowBack = new DropShadow();
        JI9_backButton.addEventHandler(MouseEvent.MOUSE_ENTERED, new EventHandler<MouseEvent>() {
          @Override
          public void handle(MouseEvent e) {
            JI9_backButton.setEffect(JI_shadowBack);
          }
      });

      JI9_backButton.addEventHandler(MouseEvent.MOUSE_EXITED, new EventHandler<MouseEvent>() {
          @Override
          public void handle(MouseEvent e) {
            JI9_backButton.setEffect(null);
          }
      });
      JI9_backButton.setOnAction(event ->{
        JI9_Stage.setScene(screen6.getScreen6Scene());
  });


        Button JI9_UPFile = new Button("Upload Your File");
        JI9_UPFile.setFont(ft);
        JI9_UPFile.setPrefWidth(450);

        DropShadow JI_shadowUpf = new DropShadow();
        JI9_UPFile.addEventHandler(MouseEvent.MOUSE_ENTERED, new EventHandler<MouseEvent>() {
          @Override
          public void handle(MouseEvent e) {
            JI9_UPFile.setEffect(JI_shadowUpf);
          }
      });

      JI9_UPFile.addEventHandler(MouseEvent.MOUSE_EXITED, new EventHandler<MouseEvent>() {
          @Override
          public void handle(MouseEvent e) {
            JI9_UPFile.setEffect(null);
          }
      });

        // JI9_UPFile.setOnAction(new javafx.event.EventHandler<ActionEvent>() {
        //     public void handle(ActionEvent Event) {
        //         System.out.println("Upload Your File");
        //     }
        // });

          JI9_UPFile.setOnAction(event -> {
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Choose a File");
            File file = fileChooser.showOpenDialog(JI9_Stage);
            if (file != null) {
                System.out.println("File selected: " + file.getAbsolutePath());
            }
        });


        Button JI9_AddPhoto = new Button("Upload Your Photo");
        JI9_AddPhoto.setFont(ft);
        JI9_AddPhoto.setPrefWidth(450);

        DropShadow JI_shadowUp = new DropShadow();
        JI9_AddPhoto.addEventHandler(MouseEvent.MOUSE_ENTERED, new EventHandler<MouseEvent>() {
          @Override
          public void handle(MouseEvent e) {
            JI9_AddPhoto.setEffect(JI_shadowUp);
          }
      });

      JI9_AddPhoto.addEventHandler(MouseEvent.MOUSE_EXITED, new EventHandler<MouseEvent>() {
          @Override
          public void handle(MouseEvent e) {
            JI9_AddPhoto.setEffect(null);
          }
      });
        // JI9_AddPhoto.setOnAction(new javafx.event.EventHandler<ActionEvent>() {
        //     public void handle(ActionEvent Event) {
        //         System.out.println("Upload Your Photo");
        //     }
        // });

        JI9_AddPhoto.setOnAction(event -> {
          FileChooser fileChooser = new FileChooser();
          fileChooser.setTitle("Choose a Photo");
          fileChooser.getExtensionFilters().addAll(
              new FileChooser.ExtensionFilter("Image Files", "*.png", "*.jpg", "*.jpeg", "*.gif")
          );
          File file = fileChooser.showOpenDialog(JI9_Stage);
          if (file != null) {
              System.out.println("Photo selected: " + file.getAbsolutePath());
          }
          String url = ImageUploader.uploadImage(file.getPath(), file.getName());
          String id =Screen4.docId;

          FirebaseConfig.updateDocumentByTheDocumentIdWithImageUrlArray(url,id);
      });



        // Layouts
        VBox JI9_VBOx = new VBox(10, JI9_label1);
        JI9_VBOx.setAlignment(Pos.CENTER);
        JI9_VBOx.setPadding(new Insets(20));


        // // Create a HBox to hold the SVG logo and the TextField
        // HBox hbox = new HBox(5);
        // hbox.getChildren().addAll(svgLogoView, textField);

        VBox JI9_VBOx2 = new VBox(20, textField, JI9_AddPhoto, JI9_UPFile);
        JI9_VBOx2.setAlignment(Pos.CENTER);
        JI9_VBOx2.setPadding(new Insets(50));

         // Load the background image
         Image backgroundImage = new Image("/images/bk1.png");
       //  BackgroundSize backgroundSize = new BackgroundSize(100, 100, true, true, true, false);

         BackgroundImage background = new BackgroundImage(
             backgroundImage,
             
             BackgroundRepeat.NO_REPEAT,
             BackgroundRepeat.NO_REPEAT,
             BackgroundPosition.CENTER,
             new BackgroundSize(BackgroundSize.AUTO, BackgroundSize.AUTO, false, false, true, true)

         );

        BorderPane root = new BorderPane();
       // root.setStyle("-fx-background-color:#FEFFD2");

        root.setBackground(new Background(background));
        root.setTop(JI9_VBOx);
        root.setCenter(JI9_VBOx2);
        root.setBottom(JI9_backButton);
        root.setPadding(new Insets(10));
      

        // Create the Scene with the BorderPane
        Scene sc = new Scene(root, 700, 500);
        return sc;
        // Set the Scene to the Stage
        //JI9_Stage.setScene(sc);
        //JI9_Stage.show();
    }

//     public static void main(String[] args) {
//         launch(args);
//     }
}
