package com.c2w.AllScreens;

import javafx.application.Platform;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;

import java.util.concurrent.CompletableFuture;

public class ShowAssignment {

    private StudentAssignment studentAssignment;
    private Stage prStage;
    private Scene scene;
    private ImageView imageView; // Add ImageView to display image

    public ShowAssignment(StudentAssignment studentAssignment, Stage prStage) {
        this.studentAssignment = studentAssignment;
        this.prStage = prStage;
        this.scene = initScene(prStage);
    }

    public Scene getShowAssigScene() {
        return scene;
    }

    public Scene initScene(Stage prStage) {
        Font ft = Font.font("Courier New", FontWeight.BLACK, 50);
        prStage.setTitle("Show Assignment");
        prStage.setWidth(1000);
        prStage.setHeight(1000);
        prStage.setFullScreen(true);

        Label JI_label = new Label("Show Assignment");
        JI_label.setAlignment(Pos.TOP_CENTER);
        JI_label.setTextFill(Color.BLACK);
        JI_label.setFont(ft);
        JI_label.setStyle("-fx-text-fill: #B3E5FC");

        imageView = new ImageView();
        imageView.setFitWidth(600);
        imageView.setFitHeight(400);
        imageView.setPreserveRatio(true);

        Button JI_back5 = new Button("Back");
        JI_back5.setFont(Font.font("Courier New", FontWeight.BOLD, 20));
    //    JI_back5.setOnAction(event -> {
       //     Scene previousScene;
            // Define the action for the back button here
         //   prStage.setScene(previousScene); // Replace with your actual previous scene
      //  });

      JI_back5.setOnAction(event ->{
        StudentAssignment std=new StudentAssignment(this, prStage);
        prStage.setScene(std.getScene());
        prStage.show();


      });

        StackPane imagePane = new StackPane(imageView);
        imagePane.setAlignment(Pos.CENTER);

        BorderPane root = new BorderPane();
        root.setTop(JI_label);
        BorderPane.setAlignment(JI_label, Pos.TOP_CENTER);
        root.setCenter(imagePane);
        root.setBottom(JI_back5);
        BorderPane.setAlignment(JI_back5, Pos.BOTTOM_LEFT);

        Scene sc = new Scene(root, 1000, 1000);

        displayImageUrl("196");  // Replace "198" with the actual document ID

        return sc;
    }

    private void displayImageUrl(String documentId) {
        CompletableFuture.supplyAsync(() -> FirebaseConfig.getImageUrls(documentId))
                .thenAccept(imageUrls -> {
                    if (imageUrls != null && !imageUrls.isEmpty()) {
                        String imageUrl = imageUrls.get(0); // Assuming you want to display the first image URL
                        Platform.runLater(() -> imageView.setImage(new Image(imageUrl)));
                    }
                })
                .exceptionally(throwable -> {
                    throwable.printStackTrace();
                    return null;
                });
    }
}
