package com.c2w.AllScreens;

import com.google.cloud.firestore.CollectionReference;
import com.google.cloud.firestore.DocumentSnapshot;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.Query;
import com.google.cloud.firestore.QueryDocumentSnapshot;
import com.google.cloud.firestore.QuerySnapshot;
import com.google.firebase.cloud.FirestoreClient;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;

import java.util.List;
import java.util.concurrent.ExecutionException;

public class ImageShow {
    private StudentAssignment studentAssignment;
    private Stage prStage;
    private Scene scene;

    public ImageShow(StudentAssignment studentAssignment, Stage prStage) {
        this.prStage = prStage;
        this.scene = initScene(prStage);
    }

    private Firestore firestore;

    public Scene getimgScene() {
        return scene;
    }

    public Scene initScene(Stage prStage) {
        firestore = FirestoreClient.getFirestore(); // Initialize Firestore

        Font ft = Font.font("Courier New", FontWeight.BOLD, 30);
        prStage.setTitle("Screen5");
        prStage.setHeight(500);
        prStage.setWidth(700);

        Line l = new Line();
        l.setStartX(0);
        l.setStartY(90);
        l.setEndX(2000);
        l.setEndY(90);
        l.setStroke(Color.BLACK);
        l.setStrokeWidth(3);

        HBox hb = new HBox(l);
        hb.setAlignment(Pos.CENTER);
        hb.setPadding(new Insets(100, 0, 0, 0)); // set line padding

        // Bottom Buttons
        Button studymaterialButton = new Button("Study Material");
        studymaterialButton.setFont(ft);
        studymaterialButton.setStyle("-fx-background-color: #194a7a; -fx-text-fill: #FFFFFF;");
        studymaterialButton.setOnMouseClicked(event -> {
            try {
                String imageUrl = fetchImageUrlFromFirestore(StudentloginScreen1.batchName, "imageUrls");
                showImageScreen(prStage, imageUrl);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        DropShadow JI_shadowforStm = new DropShadow();
        studymaterialButton.addEventHandler(MouseEvent.MOUSE_ENTERED,
                e -> studymaterialButton.setEffect(JI_shadowforStm));
        studymaterialButton.addEventHandler(MouseEvent.MOUSE_EXITED,
                e -> studymaterialButton.setEffect(null));

        // Assignment Button
        Button assignmentButton = new Button("Assignment");
        assignmentButton.setFont(ft);
        assignmentButton.setStyle("-fx-background-color: #476f95; -fx-text-fill: #FFFFFF;");

        DropShadow JI_shadowforAssi = new DropShadow();
        assignmentButton.addEventHandler(MouseEvent.MOUSE_ENTERED,
                e -> assignmentButton.setEffect(JI_shadowforAssi));
        assignmentButton.addEventHandler(MouseEvent.MOUSE_EXITED,
                e -> assignmentButton.setEffect(null));

        assignmentButton.setOnMouseClicked(event -> {
            try {
                String imageUrl = fetchImageUrlFromFirestore(StudentloginScreen1.batchName, "imageUrls");
                showImageScreen(prStage, imageUrl);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        Button JI_back5 = new Button("Back");
        JI_back5.setFont(ft);
        JI_back5.setAlignment(Pos.BOTTOM_LEFT);
        JI_back5.setStyle(
                "-fx-background-color: rgba(230, 230, 250, 0.8); " +
                        "-fx-background-radius: 30; " + // Rounded corners
                        "-fx-border-radius: 30; " + // Border radius
                        "-fx-border-color: black; " + // Optional border color
                        "-fx-border-width: 2;" // Optional border width
        );

        HBox bottBox = new HBox(30, JI_back5, studymaterialButton, assignmentButton);
        bottBox.setAlignment(Pos.CENTER);
        bottBox.setPadding(new Insets(40));

        Pane JI_p2 = new Pane();
        JI_p2.getChildren().addAll(l);
        JI_p2.setLayoutX(1000);
        JI_p2.setLayoutY(100);

        BorderPane root = new BorderPane();
        Image backgroundImage = new Image("/images/bk1.png");
        BackgroundImage background = new BackgroundImage(
                backgroundImage,
                BackgroundRepeat.NO_REPEAT,
                BackgroundRepeat.NO_REPEAT,
                BackgroundPosition.DEFAULT,
                new BackgroundSize(BackgroundSize.AUTO, BackgroundSize.AUTO, false, false, true, true)
        );

        root.setBackground(new Background(background));
        root.setBottom(bottBox);
        root.setTop(JI_p2);

        scene = new Scene(root, 500, 700);
        return scene;
    }

    public String fetchImageUrlFromFirestore(String batchName, String fieldName) throws ExecutionException, InterruptedException {
        CollectionReference classesCollection = firestore.collection("classes");

        // Create a query to find the document with the specified batch name
        Query query = classesCollection.whereEqualTo("batchName", batchName);
        QuerySnapshot querySnapshot = query.get().get();

        // Check if we have any matching documents
        List<QueryDocumentSnapshot> documents = querySnapshot.getDocuments();
        if (!documents.isEmpty()) {
            // Assuming there's only one document with the matching batch name
            DocumentSnapshot documentSnapshot = documents.get(0);
            List<String> imageUrls = (List<String>) documentSnapshot.get("imageUrls");
            if (imageUrls != null && !imageUrls.isEmpty()) {
                return imageUrls.get(0); // Return the first URL or handle as needed
            } else {
                throw new IllegalArgumentException("No image URLs found in document for batch name: " + batchName);
            }
        } else {
            // Handle the case where no document matches the batch name
            throw new IllegalArgumentException("No document found with batch name: " + batchName);
        }
    }

    private void showImageScreen(Stage prStage, String imageUrl) {  
        Image image = new Image(imageUrl);
        ImageView imageView = new ImageView(image);
        imageView.setFitWidth(600);
        imageView.setFitHeight(400);
        imageView.setPreserveRatio(true);

        StackPane imageRoot = new StackPane(imageView);
        Scene imageScene = new Scene(imageRoot, 700, 500);

        prStage.setScene(imageScene);
    }
}
       


