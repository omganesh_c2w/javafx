package com.c2w.AllScreens;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import com.google.api.core.ApiFuture;
import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.DocumentSnapshot;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.WriteResult;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;

public class Screen3 {

    private Screen2 screen2;
    private Stage sc1Stage;
    private Scene curScene2;
    private static Firestore db;

    public Screen3(Screen2 screen2, Stage sc1Stage) {
        this.screen2 = screen2;
        this.sc1Stage = sc1Stage;
        this.curScene2 = initialize(sc1Stage);
    }

    public Scene getScreen3Scene() {
        return curScene2;
    }

    public Scene initialize(Stage prStage3) {
        prStage3.setTitle("Screen3");
        prStage3.setWidth(800);
        prStage3.setHeight(600);
        prStage3.setFullScreen(true);

        Font ft = Font.font("Courier New", FontWeight.BOLD, 20);



         // Background image
        Image bkimg = new Image("/images/bk1.png");
        ImageView bkimgview = new ImageView(bkimg);
        bkimgview.setPreserveRatio(true);
        bkimgview.setFitWidth(3000);
        bkimgview.setFitHeight(1200);
        bkimgview.setMouseTransparent(true); // This line makes the image view not intercept mouse events

        // Creating Labels and TextFields for class information
        Label classNameLabel = new Label("Class Name:");
        classNameLabel.setFont(ft);
        TextField classNameTextField = new TextField();
        classNameTextField.setPrefWidth(600);
        classNameTextField.setFont(ft);
        classNameTextField.setPromptText("Enter Your Class Name");

        Label batchLabel = new Label("Batch:");
        batchLabel.setFont(ft);
        TextField batchTextField = new TextField();
        batchTextField.setPrefWidth(600);
        batchTextField.setFont(ft);
        batchTextField.setPromptText("Enter Your Batch");

        Label subjectLabel = new Label("Subject:");
        subjectLabel.setFont(ft);
        TextField subjectTextField = new TextField();
        subjectTextField.setPrefWidth(600);
        subjectTextField.setFont(ft);
        subjectTextField.setPromptText("Enter Your Subject Here");

        Label teacherNameLabel = new Label("Teacher Name:");
        teacherNameLabel.setFont(ft);
        TextField teacherNameTextField = new TextField();
        teacherNameTextField.setPrefWidth(600);
        teacherNameTextField.setFont(ft);
        teacherNameTextField.setPromptText("Enter Teacher Name Here");

        Label codeLabel = new Label("Code:");
        codeLabel.setFont(ft);
        TextField codeTextField = new TextField();
        codeTextField.setPrefWidth(600);
        codeTextField.setFont(ft);

        // Creating GridPane for center alignment
        GridPane gridPane = new GridPane();
        gridPane.setAlignment(Pos.CENTER);
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        gridPane.setPadding(new Insets(20));

        // Labels and TextFields for the GridPane
        gridPane.add(classNameLabel, 0, 0);
        gridPane.add(classNameTextField, 1, 0);
        gridPane.add(batchLabel, 0, 1);
        gridPane.add(batchTextField, 1, 1);
        gridPane.add(subjectLabel, 0, 2);
        gridPane.add(subjectTextField, 1, 2);
        gridPane.add(teacherNameLabel, 0, 3);
        gridPane.add(teacherNameTextField, 1, 3);
        gridPane.add(codeLabel, 0, 4);
        gridPane.add(codeTextField, 1, 4);

        // Create a Button
        Button JI_subBut = new Button("Submit");
        JI_subBut.setFont(ft);
        JI_subBut.setPrefWidth(150);
        JI_subBut.setPrefHeight(50);
        
      //  JI_subBut.setLayoutX(250);
      JI_subBut.setStyle(
        "-fx-background-color: rgba(230, 230, 250, 0.8); " +
        "-fx-background-radius: 30; " + // Rounded corners
        "-fx-border-radius: 30; " + // Border radius
        "-fx-border-color: black; " + // Optional border color
        "-fx-border-width: 2;" // Optional border width
    );

        JI_subBut.setOnAction(event -> {
            String className = classNameTextField.getText();
            String batch = batchTextField.getText();
            String subject = subjectTextField.getText();
            String teacherName = teacherNameTextField.getText();
            String code = codeTextField.getText();

            System.out.println("Class Name: " + className);
            System.out.println("Batch: " + batch);
            System.out.println("Subject: " + subject);
            System.out.println("Teacher Name: " + teacherName);
            System.out.println("Code: " + code);

            Map<String, Object> data = new HashMap<>();
            data.put("name", className);
            data.put("batch", batch);
            data.put("subject", subject);
            data.put("teacherName", teacherName);
            data.put("code", code);



            ApiFuture<WriteResult> writeResult;
            try {
                writeResult = FirebaseConfig.writeDocument("classes", data);
            } catch (Exception e) {
                e.printStackTrace();
            }

            Screen4 screen4 = new Screen4(this, sc1Stage);
            prStage3.setScene(screen4.getScreen4Scene());
            prStage3.show();
        });

        Button JI_back1 = new Button("Back");
        JI_back1.setFont(ft);
        JI_back1.setPrefWidth(150);
        JI_back1.setPrefHeight(50);
        JI_back1.setStyle(
            "-fx-background-color: rgba(230, 230, 250, 0.8); " +
            "-fx-background-radius: 30; " + // Rounded corners
            "-fx-border-radius: 30; " + // Border radius
            "-fx-border-color: black; " + // Optional border color
            "-fx-border-width: 2;" // Optional border width
        );
        
        JI_back1.setOnAction(event -> {
            prStage3.setScene(screen2.getScreen2Scene());
        });
        HBox buttonBox = new HBox(10);
        buttonBox.setAlignment(Pos.CENTER);
        buttonBox.getChildren().addAll(JI_subBut, JI_back1);
        gridPane.add(buttonBox, 0, 5, 2, 1);        


       // gridPane.add(JI_subBut, 0, 5, 2, 1);
       // gridPane.add(JI_back1, 0, 5, 2, 1);

        BorderPane bp1 = new BorderPane();
        bp1.setCenter(bkimgview);
        
        StackPane sp = new StackPane(bp1);
        sp.getChildren().addAll(gridPane);
        

        
        Scene scene = new Scene(sp, 600, 400);
        return scene;
    }

    public VBox readRec() throws InterruptedException, ExecutionException {
        VBox vb1 = new VBox();
        DocumentReference docRef = FirebaseConfig.db.collection("classes").document();
        ApiFuture<DocumentSnapshot> snapShot = docRef.get();
        DocumentSnapshot docSnap = snapShot.get();
        if (docSnap.exists()) {
            Label l1 = new Label(docSnap.getString("name"));
            Label l2 = new Label(docSnap.getString("batch"));
            Label l3 = new Label(docSnap.getString("teacherName"));
            vb1.getChildren().addAll(l1, l2, l3);
        } else {
            System.out.println("Document Not Found");
        }
        return vb1;
    }
}
