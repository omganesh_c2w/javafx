package com.c2w.AllScreens;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class AboutUs  {

    private LandingPage landingPage;
    private Stage sc1Stage;
    private Scene curscene;

    public AboutUs(LandingPage landingPage,Stage sc1Stage ){
        this.landingPage=landingPage;
        this.sc1Stage=sc1Stage;
        this.curscene=initialize(sc1Stage);
    }

    public Scene getSceneabtScene(){
        return curscene;
    }

    public Scene initialize(Stage prstage){

        prstage.setTitle("About us");
        prstage.setWidth(1000);
        prstage.setHeight(1000);
        prstage.setFullScreen(true);


        Label lb=new Label("Hello");

        Image img=new Image("/images/aboutus.jpeg");
        ImageView imageView=new ImageView(img);
        imageView.setPreserveRatio(true);
        imageView.fitWidthProperty().bind(prstage.widthProperty());
        imageView.fitHeightProperty().bind(prstage.heightProperty());

        
        BorderPane bp=new BorderPane();
        bp.setCenter(imageView);

        Scene scene=new Scene(bp, 500,700);
        return scene;
     //   prstage.setScene(scene);
        

      //  prstage.show();
        
    }

    
    
}
