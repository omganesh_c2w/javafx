package com.c2w.AllScreens;

import java.util.List;
import java.util.concurrent.ExecutionException;

import com.google.api.core.ApiFuture;
import com.google.cloud.firestore.DocumentSnapshot;
import com.google.cloud.firestore.QueryDocumentSnapshot;
import com.google.cloud.firestore.QuerySnapshot;

import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class StudentYourClass {
     private StudentJoinClass joinClass;
    private Stage sc1Stage;
    private Scene curScene3;

    public StudentYourClass(StudentJoinClass joinClass, Stage sc1Stage) {
        this.joinClass = joinClass;
        this.sc1Stage = sc1Stage;
        this.curScene3 = initialize(sc1Stage);
    }

    public Scene getScreen4Scene() {
        return curScene3;
    }

    Font ft = Font.font("Courier New", FontWeight.BOLD, 20);

    public Scene initialize(Stage prStage4) {
        prStage4.setTitle("Screen4");
        prStage4.setHeight(500);
        prStage4.setWidth(700);
        prStage4.setResizable(true);
        prStage4.setFullScreen(true);

        Circle JI_CP = new Circle(200);

        Image JI_image2 = new Image("images/file.png");
        ImageView JI_view2 = new ImageView(JI_image2);
        JI_view2.setPreserveRatio(true);
        JI_view2.setFitHeight(70);
        JI_view2.setFitWidth(70);

        JI_view2.setOnMouseClicked(event -> System.out.println("Create The Class Room"));

        Button JIaddButton = new Button();
        JIaddButton.setShape(JI_CP);
        JIaddButton.setGraphic(JI_view2);
        JIaddButton.setAlignment(Pos.BOTTOM_RIGHT);

        DropShadow JI_shadowadd = new DropShadow();
        JIaddButton.addEventHandler(MouseEvent.MOUSE_ENTERED, e -> JIaddButton.setEffect(JI_shadowadd));
        JIaddButton.addEventHandler(MouseEvent.MOUSE_EXITED, e -> JIaddButton.setEffect(null));

        Line line = new Line();
        line.setStartX(0);
        line.setStartY(0);
        line.endXProperty().bind(prStage4.widthProperty());
        line.setEndY(0);
        line.setStrokeWidth(3);

        Text tx1 = new Text("Your Classes");
        tx1.setFont(ft);

        VBox vb = new VBox();
        vb.setAlignment(Pos.TOP_CENTER);
        vb.getChildren().addAll(tx1, line);
        vb.setPadding(new Insets(30, 0, 0, -15));

        VBox classesBox = new VBox(10);
        classesBox.setPadding(new Insets(10));
        classesBox.setAlignment(Pos.CENTER);

        // Fetch classes from Firestore
        fetchClassesFromFirestore(classesBox);

        Button JI_back2 = new Button("Back");
        JI_back2.setFont(ft);
        JI_back2.setAlignment(Pos.BOTTOM_LEFT);
        JI_back2.setOnAction(event -> {
            sc1Stage.setScene(StudentYourClass.getjoinScene());
            System.out.println("Back");
        });

        BorderPane root = new BorderPane();
        root.setCenter(vb);
        root.setStyle("-fx-background-color:#FEFFD2");
        root.setCenter(classesBox);

        Pane backPane = new Pane(JI_back2);
        backPane.getChildren().addAll();
        backPane.setLayoutX(200);
        backPane.setLayoutY(300);

        Image backgroundImage = new Image("images/logof.png");
        BackgroundImage background = new BackgroundImage(
            backgroundImage,
            BackgroundRepeat.NO_REPEAT,
            BackgroundRepeat.NO_REPEAT,
            BackgroundPosition.DEFAULT,
            BackgroundSize.DEFAULT
        );
        root.setBackground(new Background(background));

        StackPane buttom = new StackPane(JIaddButton);
        buttom.setAlignment(Pos.BOTTOM_RIGHT);
        buttom.setPadding(new Insets(50));
        root.setBottom(buttom);
        root.setPadding(new Insets(5));
        root.setBottom(backPane);

        Scene scene = new Scene(root, 500, 200);
        return scene;
    }

    private void fetchClassesFromFirestore(VBox classesBox) {
        ApiFuture<QuerySnapshot> future = FirebaseConfig.readCollection("classes");
        future.addListener(() -> {
            try {
                List<QueryDocumentSnapshot> documents = future.get().getDocuments();
                Platform.runLater(() -> {
                    for (DocumentSnapshot document : documents) {
                        VBox classBox = new VBox(10);
                        classBox.setPadding(new Insets(10));
                        classBox.setPrefSize(150, 150);
                        classBox.setAlignment(Pos.CENTER);
                        classBox.setStyle("-fx-border-color: black; -fx-border-width: 2; -fx-border-style: solid;");

                        Label classNameLabel = new Label("Class Name: " + document.getString("name"));
                        Label batchLabel = new Label("Batch: " + document.getString("batch"));
                        Label teacherNameLabel = new Label("Teacher Name: " + document.getString("teacherName"));

                        classBox.getChildren().addAll(classNameLabel, batchLabel, teacherNameLabel);

                        classBox.setOnMouseClicked(event -> {
                            System.out.println("Class clicked: " + document.getString("name"));
                            Screen5 screen5 = new Screen5(this, sc1Stage);
                            sc1Stage.setScene(screen5.getScreen5Scene());
                            sc1Stage.show();
                        });

                        classesBox.getChildren().add(classBox);
                    }
                });
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        }, Runnable::run);
    }
    
}
