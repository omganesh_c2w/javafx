package com.c2w.AllScreens;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class LandingPage extends Application {
    
    private Stage primaryStage;
    private static Scene scene;
    
    public void setStage(Scene newScene) {
        primaryStage.setScene(newScene);
    }

    public static Scene getLandingPageScene() {
        return scene; 
    }

    public void start(Stage prStage) {
        
        prStage.setTitle("Screen1");
        prStage.setWidth(500);
        prStage.setHeight(700);
        prStage.setMaximized(true);

        Image bkimg = new Image("images/landing.jpg");
        ImageView bkimgview = new ImageView(bkimg); 
        bkimgview.setPreserveRatio(true);
        bkimgview.setFitWidth(2800);
        bkimgview.setFitHeight(1100);

        // Create the "Get Started" button
        Button getStartedButton = new Button("<Get Started>");
        getStartedButton.setFont(javafx.scene.text.Font.font("Courier New", javafx.scene.text.FontWeight.BOLD, 30));
        getStartedButton.setStyle("-fx-background-color: transparent; -fx-border-color: transparent; -fx-text-fill: WHITE; -fx-font-size: 40; -fx-font-weight: BOLD;");
        StackPane.setMargin(getStartedButton, new Insets(0, 0, 10, 0)); // Adjusted margin

        DropShadow JI_shadowadd = new DropShadow();
        getStartedButton.addEventHandler(MouseEvent.MOUSE_ENTERED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent e) {
                getStartedButton.setEffect(JI_shadowadd);
            }
        });

        getStartedButton.addEventHandler(MouseEvent.MOUSE_EXITED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent e) {
                getStartedButton.setEffect(null);
            }
        });

        getStartedButton.setOnAction(event -> {
            LoginController obLoginScreen = new LoginController(this, primaryStage);
            primaryStage.setScene(obLoginScreen.getLoginScreenScene());
            primaryStage.show();
        }); 

        // Create the "About Us" button
        Button aboutusButton = new Button("<About Us>");
        aboutusButton.setFont(javafx.scene.text.Font.font("Courier New", javafx.scene.text.FontWeight.BOLD, 30));
        aboutusButton.setStyle("-fx-background-color: transparent; -fx-border-color: transparent; -fx-text-fill: WHITE; -fx-font-size: 40; -fx-font-weight: BOLD;");

        DropShadow JI_shadowadd1 = new DropShadow();
        aboutusButton.addEventHandler(MouseEvent.MOUSE_ENTERED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent e) {
                aboutusButton.setEffect(JI_shadowadd1);
            }
        });
        aboutusButton.addEventHandler(MouseEvent.MOUSE_EXITED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent e) {
                aboutusButton.setEffect(null);
            }
        });
        aboutusButton.setOnAction(event ->{
            AboutUs abt=new AboutUs(this, prStage);
            prStage.setScene(abt.getSceneabtScene());

            
        });

        // Create a VBox to stack the buttons vertically
        VBox buttonBox = new VBox(10, getStartedButton, aboutusButton);
        buttonBox.setAlignment(Pos.BOTTOM_CENTER);
        buttonBox.setPadding(new Insets(0, 10, 100, 0)); // Adjust padding as needed

        

        // Create a StackPane to overlay the VBox and ImageView
        StackPane stackPane = new StackPane();
        stackPane.getChildren().addAll(bkimgview, buttonBox);

        this.primaryStage = prStage;

        Scene scene = new Scene(stackPane, 500, 700);
        prStage.setScene(scene);
        prStage.show();
    }
}
