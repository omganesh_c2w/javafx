package com.c2w.AllScreens;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.URI;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.auth.UserRecord;

import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

public class FirebaseConfigProxgy {
    public static void initialize() {
        try {
            FileInputStream serviceAccount =
                new FileInputStream("src/main/resources/proxylogin.json");

            FirebaseOptions options = FirebaseOptions.builder()
                .setCredentials(GoogleCredentials.fromStream(serviceAccount))
                .build();

            FirebaseApp.initializeApp(options);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}       