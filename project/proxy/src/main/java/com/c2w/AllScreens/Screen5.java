package com.c2w.AllScreens;

import com.c2w.Attendance.AttendanceTable;
import com.google.cloud.storage.Acl.Group;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;

public class Screen5  {
    private Screen4 screen4;

    private Stage sc1Stage;
    private static Scene curScene4;
    private StudentTableView studentTableView;


    public Screen5(Screen4 screen4, Stage sc1Stage) {
        this.screen4=screen4;
        this.sc1Stage=sc1Stage;
        this.curScene4=initialize(sc1Stage);
        
    }
    public Screen5(StudentTableView studentTableView,Stage sc1Stage ){
        this.studentTableView=studentTableView;
        this.sc1Stage=sc1Stage;
        this.curScene4=initialize(sc1Stage);
    }
     public static Scene getScreen5Scene(){
        return curScene4;
    }

 
        public Scene initialize (Stage prStage) {
            Font ft=Font.font("Courier New",FontWeight.BOLD,30);
            prStage.setTitle("Screeen5");
            prStage.setHeight(500);
            prStage.setWidth(700);
            prStage.setFullScreen(true);
            Line l=new Line();
            l.setStartX(0);
            l.setStartY(90);
            l.setEndX(2000);
            l.setEndY(90);
            l.setStroke(Color.BLACK); 
            l.setStrokeWidth(3);
     
            HBox hb=new HBox(l);
            hb.setAlignment(Pos.CENTER);
            hb.setPadding(new Insets(100,0,0,0)); //set line padding

            
            // Bottom Buttons 
            Button studymatrialBotton=new Button("Study Material");
            studymatrialBotton.setLayoutX(100);
            studymatrialBotton.setLayoutY(900);
            studymatrialBotton.setFont(ft);
            studymatrialBotton.setStyle("-fx-background-color: #194a7a; -fx-text-fill: #FFFFFF;");
            studymatrialBotton.setOnMouseClicked(event ->{
                System.out.println("Study Material");
                Screen6 screen6=new Screen6(this, sc1Stage);
                prStage.setScene(screen6.getScreen6Scene());
                prStage.show();
            });
    
            DropShadow JI_shadowforStm = new DropShadow();
    
            studymatrialBotton.addEventHandler(MouseEvent.MOUSE_ENTERED, 
        new EventHandler<MouseEvent>() {
             public void handle(MouseEvent e) {
                studymatrialBotton.setEffect(JI_shadowforStm);
            }
    });
    studymatrialBotton.addEventHandler(MouseEvent.MOUSE_EXITED, 
    new EventHandler<MouseEvent>() {
         public void handle(MouseEvent e) {
            studymatrialBotton.setEffect(null);
        }
    });         
            //Assignment Button
            Button assginmentButton=new Button("Assignment");
            assginmentButton.setLayoutX(400);
            assginmentButton.setLayoutY(900);
            assginmentButton.setFont(ft);
           assginmentButton.setStyle("-fx-background-color: #476f95; -fx-text-fill: #FFFFFF;");
           assginmentButton.setOnMouseClicked(event ->{
            Screen6 screen6=new Screen6(this, sc1Stage);
                prStage.setScene(screen6.getScreen6Scene());
                prStage.show();

           });
    
            DropShadow JI_shadowforAssi = new DropShadow();
            assginmentButton.addEventHandler(MouseEvent.MOUSE_ENTERED, 
        new EventHandler<MouseEvent>() {
             public void handle(MouseEvent e) {
                assginmentButton.setEffect(JI_shadowforAssi);
            }
    });
    assginmentButton.addEventHandler(MouseEvent.MOUSE_EXITED, 
    new EventHandler<MouseEvent>() {
         public void handle(MouseEvent e) {
            assginmentButton.setEffect(null);
        }
    });
            Button studeButton=new Button("Student");
            studeButton.setLayoutX(650);
            studeButton.setLayoutY(900);
            studeButton.setFont(ft);
            studeButton.setStyle("-fx-background-color:#7593af ; -fx-text-fill: #FFFFFF;");
    
            DropShadow JI_shadowforstud = new DropShadow();
            studeButton.addEventHandler(MouseEvent.MOUSE_ENTERED, 
             new EventHandler<MouseEvent>() {
             public void handle(MouseEvent e) {
                studeButton.setEffect(JI_shadowforstud);
            }
    });
    studeButton.addEventHandler(MouseEvent.MOUSE_EXITED, 
    new EventHandler<MouseEvent>() {
         public void handle(MouseEvent e) {
            studeButton.setEffect(null);
        }
    });

    studeButton.setOnMouseClicked(event ->{
       
        StudentTableView std = new StudentTableView(prStage);
        prStage.setScene(std.getScene());
    });
            Button messButton=new Button("Message");
            messButton.setLayoutX(900);
            messButton.setLayoutY(900);
            messButton.setFont(ft);
            messButton.setStyle("-fx-background-color:#a3b7ca ; -fx-text-fill: #FFFFFF;");
      
            DropShadow shadow = new DropShadow();
            messButton.addEventHandler(MouseEvent.MOUSE_ENTERED, 
        new EventHandler<MouseEvent>() {
             public void handle(MouseEvent e) {
                messButton.setEffect(shadow);
            }
    });

    
    messButton.addEventHandler(MouseEvent.MOUSE_EXITED, 
    new EventHandler<MouseEvent>() {
         public void handle(MouseEvent e) {
            messButton.setEffect(null);
        }
    });

    Button JI_back5 = new Button("Back");
    JI_back5.setFont(ft);
    JI_back5.setAlignment(Pos.BOTTOM_LEFT);
    JI_back5.setStyle(
    "-fx-background-color: rgba(230, 230, 250, 0.8); " +
    "-fx-background-radius: 30; " + // Rounded corners
    "-fx-border-radius: 30; " + // Border radius
    "-fx-border-color: black; " + // Optional border color
    "-fx-border-width: 2;" // Optional border width
);
JI_back5.setOnAction(event -> {
        sc1Stage.setScene(screen4.getScreen4Scene());
        System.out.println("Back");
    });
            HBox bottBox=new HBox(30,JI_back5,studymatrialBotton,assginmentButton, studeButton,messButton);
            bottBox.setAlignment(Pos.CENTER);
            bottBox.setPadding(new Insets(40));

        
            Circle JI_c=new Circle(200);
          
            JI_c.setCenterX(100);
            JI_c.setCenterY(100);
            JI_c.setStroke(Color.BLACK);
            
            Button JI_Fb = new Button("A");
            JI_Fb.setShape(JI_c);
            JI_Fb.setLayoutX(1550);
            JI_Fb.setLayoutY(21);
            JI_Fb.setMaxHeight(100);
            JI_Fb.setMinSize(40, 40);
            JI_Fb.setFont(ft);
      
            DropShadow JI_ShadowforA = new DropShadow();
            JI_Fb.addEventHandler(MouseEvent.MOUSE_ENTERED, 
                new EventHandler<MouseEvent>() {
                    public void handle(MouseEvent e) {
                        JI_Fb.setEffect(JI_ShadowforA);
                    }
            });

            JI_Fb.setOnAction(event->{
                AttendanceTable attendanceTable = new AttendanceTable(prStage);
                prStage.setScene(attendanceTable.getScene());
            });

      
            Circle JI_c2=new Circle(200);
            JI_c2.setCenterX(100);
            JI_c2.setCenterY(100);
            
            JI_c2.setStroke(Color.BLACK);
            
            Button JI_i = new Button("LOGOUT");
            JI_i.setShape(JI_c2);
            JI_i.setLayoutX(1700);
            JI_i.setLayoutY(20);
            JI_i.setMinSize(40,40);
            JI_i.setFont(ft);

            JI_i.setOnAction(event ->{
                LoginController loginController=new LoginController(this,prStage);
                prStage.setScene(loginController.getLoginScreenScene());
                prStage.show();


        });

            Pane JI_p2=new Pane();
               JI_p2.getChildren().addAll(JI_Fb,JI_i,l);
               JI_p2.setLayoutX(1000);
              JI_p2.setLayoutY(100);
            
    
              StackPane JI_SP=new StackPane();
              JI_SP.getChildren().addAll();
              
              
    
          BorderPane root=new BorderPane();
          //  root.setStyle("-fx-background-color:#FEFFD2");
            Image backgroundImage = new Image("/images/bk1.png");
             BackgroundImage background = new BackgroundImage(
              backgroundImage,
              BackgroundRepeat.NO_REPEAT,
              BackgroundRepeat.NO_REPEAT,
              BackgroundPosition.DEFAULT,
              new BackgroundSize(BackgroundSize.AUTO, BackgroundSize.AUTO, false, false, true, true)
              );
              
    
                root.setBackground(new Background(background));
              //  root.getChildren().addAll(JI_Fb,JI_i);
                root.setBottom(bottBox);
              //  root.setTop(hb);
                root.setTop(JI_p2);
                
    
    
        //   Pane JI_p2=new Pane();
        //   JI_p2.getChildren().addAll(JI_Fb,JI_i);
        //   JI_p2.setLayoutX(1000);
        //   JI_p2.setLayoutY(100);
        //   JI_p2.getChildren().addAll(root);

            Scene scene = new Scene(root,500,700);

            return scene;
          //  prStage.setScene(scene);
            // prStage.show();
        }
    }
    

