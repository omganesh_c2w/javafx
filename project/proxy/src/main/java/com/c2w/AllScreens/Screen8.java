package com.c2w.AllScreens;


import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;

import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class Screen8  {
  private Stage sc1Stage;
  private  Scene curScene6;
  private Screen6 screen6;

  public Screen8(Screen6 screen6,Stage sc1Stage){
    this.screen6=screen6;
    this.sc1Stage=sc1Stage;
    this.curScene6=initialize(sc1Stage);

  }
    public Scene getScreen8Scene(){
      return curScene6;
    }


 public Scene initialize (Stage prStage) {
        Font ft=Font.font("Courier New",FontWeight.BOLD,30);
        prStage.setTitle("Assignment");
        prStage.setHeight(500);
        prStage.setWidth(700);
        prStage.setFullScreen(true); 

        Line l=new Line();
        l.setStartX(0);
        l.setStartY(70);
        l.setEndX(1500);
        l.setEndY(70);
        l.setStrokeWidth(3);

        
        Circle JI_arrow = new Circle(250);

        Text tx1 = new Text("Assignment");                
        tx1.setFont(ft);  

        VBox vb=new VBox();
        
         vb. getChildren().addAll(tx1,l);
         vb.setAlignment(Pos.TOP_CENTER);
        vb.setPadding(new Insets(50,0,0,0));

        Button studymatrialBotton=new Button("Study Material");
        studymatrialBotton.setLayoutX(100);
        studymatrialBotton.setLayoutY(900);
        studymatrialBotton.setFont(ft);

        Button assginmentButton=new Button("Assignment");
        assginmentButton.setLayoutX(400);
        assginmentButton.setLayoutY(900);
        assginmentButton.setFont(ft);

        Button studeButton=new Button("Student");
        studeButton.setLayoutX(650);
        studeButton.setLayoutY(900);
        studeButton.setFont(ft);

        Button messButton=new Button("Message");
        messButton.setLayoutX(900);
        messButton.setLayoutY(900);
        messButton.setFont(ft);

        HBox bottBox=new HBox(30,studymatrialBotton,assginmentButton, studeButton,messButton);
        bottBox.setAlignment(Pos.CENTER);
        bottBox.setPadding(new Insets(40));

        Label label2 = new Label("Add Your File");
        label2.setLayoutX(900);
        label2.setLayoutY(300);
        label2.setFont(ft);
        label2.setTextFill(Color.BLACK);

        Button arrowButton = new Button("⬆");
      //  arrowButton.setStyle("-fx-background-color: #d1dbe4; -fx-text-fill: white;"); // Aqua background, white text
        arrowButton.setFont(ft);
        arrowButton.setLayoutX(980);
        arrowButton.setLayoutY(375);
        arrowButton.setShape(JI_arrow);

       VBox topVBox = new VBox(10, label2, arrowButton);
       topVBox.setAlignment(Pos.CENTER);
       topVBox.setPadding(new Insets(20));


        BorderPane root=new BorderPane();
        root.setBottom(bottBox);
        root.setCenter(topVBox);

        StackPane JI_sp=new StackPane();
        JI_sp.getChildren().addAll(vb,root);
        
        Scene sc=new Scene(JI_sp,500,700);
      //  prStage.setScene(sc);        
        //prStage.show();
     }
    
}
