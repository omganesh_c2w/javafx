
import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;

import java.util.List;

public class StudentloginScreen1 {

    private LoginController loginController;
    private Stage prStage;
    private Scene scene;
    private StudentAssignment studentAssignment;
    public static String batchName;

    public StudentloginScreen1(LoginController loginController, Stage prStage) {
        this.loginController = loginController;
        this.prStage = prStage;
        this.scene = initScene(prStage);
    }

    public StudentloginScreen1(StudentAssignment studentAssignment,Stage prStage ){
        this.studentAssignment=studentAssignment;
        this.prStage=prStage;
        this.scene = initScene(prStage);
    }

    public Scene getstudentyScene(){
        return scene;
    }

    public Scene initScene(Stage prStage) {
        Font ft = Font.font("Courier New", FontWeight.BOLD, 50);

        prStage.setTitle(" Student Screen1");
        prStage.setWidth(500);
        prStage.setHeight(700);
        prStage.setFullScreen(true);

        Label JI_label = new Label("Join Class");
        JI_label.setFont(ft);
        JI_label.setStyle("-fx-text-fill: #B3E5FC");

        // Create the TextField for entering the classroom code
        TextField classCodeInput = new TextField();
        classCodeInput.setPromptText("Enter Your ClassRoom Code");
        classCodeInput.setFont(Font.font("Courier New", FontWeight.NORMAL, 15));
        classCodeInput.setPrefWidth(150);
        classCodeInput.setPrefHeight(25);
        classCodeInput.setMinWidth(300);
        classCodeInput.setMaxWidth(100);
        classCodeInput.setMinHeight(20);
        classCodeInput.setMaxHeight(30);

        // Create a HBox to center the TextField
        HBox hbox = new HBox(classCodeInput);
        hbox.setAlignment(Pos.CENTER);

        // Create the Submit button
        Button submitButton = new Button("Submit");
        submitButton.setFont(Font.font("Courier New", FontWeight.BOLD, 20));
        submitButton.setStyle("-fx-background-color: blue; -fx-text-fill: white;");

        submitButton.setOnAction(event -> {
            FirebaseConfig.addStudentToClass(classCodeInput.getText(), LoginController.userName);
            System.out.println("dfcgvhbjn   "+prStage);
            System.out.println(LoginController.userName);
            updateClassList(LoginController.userName);
            prStage.setScene(new Scene(mainLayout));
        });

        // Create a VBox to stack the HBox and Button
        VBox vb = new VBox(20, hbox, submitButton);
        vb.setAlignment(Pos.CENTER);

        Image bkimg = new Image("/images/bk1.png"); // Ensure the path is correct
        ImageView bkimgview = new ImageView(bkimg);
        bkimgview.setPreserveRatio(true);
        bkimgview.setFitWidth(3000);
        bkimgview.setFitHeight(1200);

        // Create a BorderPane to place the Label at the top center
        BorderPane bp = new BorderPane();
        bp.getChildren().add(bkimgview);
        bp.setTop(JI_label);
        bp.setCenter(vb);
        bp.setAlignment(JI_label, Pos.TOP_CENTER);

        Scene sc = new Scene(bp, 500, 700);
        prStage.show();
        updateClassList(LoginController.userName); // Update the class list initially
        return sc;
    }
    private BorderPane mainLayout = new BorderPane();; // This should be initialized somewhere in your controller\

    


    // private void updateClassList(String studentId) {
    //     List<String> classes = FirebaseConfig.getStudentClasses(studentId);
    //     if (classes != null && !classes.isEmpty()) {
    //         VBox classListVBox = new VBox(10);
    //         classListVBox.setAlignment(Pos.CENTER);
    //         for (String classCode : classes) {
    //             Label classLabel = new Label(classCode);
    //             classLabel.setFont(Font.font("Courier New", FontWeight.NORMAL, 15));
    //             classListVBox.getChildren().add(classLabel);
    //         }
    //         mainLayout.setBottom(classListVBox);
    //         BorderPane.setAlignment(classListVBox, Pos.CENTER);
    //     }
    // }  



    private void updateClassList(String studentId) {
        // Debug print to check the studentId
        System.out.println("Updating class list for studentId: " + studentId);
    
        List<String> classes = FirebaseConfig.getStudentClasses(studentId);
        
        // Debug print to check if classes are retrieved correctly
        System.out.println("Classes retrieved: " + classes);
    
        if (mainLayout == null) {
            System.out.println("mainLayout is null");
        }
    
        if (classes != null && !classes.isEmpty()) {
            VBox classListVBox = new VBox(10);
            classListVBox.setAlignment(Pos.CENTER);
            for (String classCode : classes) {
                Label classLabel = new Label(classCode);

                Button batch_button = new Button();
                batch_button.setGraphic(classLabel);
                batch_button.setOnAction(event ->{
                    batchName=batch_button.getText();
                    StudentAssignment studentassi=new StudentAssignment(this,prStage);
                    prStage.setScene(studentassi.getScene());

                });

                classLabel.setFont(Font.font("Courier New", FontWeight.BOLD,30 ));
                classListVBox.getChildren().add(batch_button);
            }
            mainLayout.setCenter(classListVBox);  // Check if mainLayout is null
            BorderPane.setAlignment(classListVBox, Pos.CENTER);
        } else {
            // Handle the case where no classes are found or classes is null
            Label noClassesLabel = new Label("No classes joined.");
            noClassesLabel.setFont(Font.font("Courier New", FontWeight.NORMAL, 30));
            VBox classListVBox = new VBox(10, noClassesLabel);
            classListVBox.setAlignment(Pos.CENTER);
            mainLayout.setBottom(classListVBox);  // Check if mainLayout is null
            BorderPane.setAlignment(classListVBox, Pos.CENTER);
        }
    }
    

    public Scene getScene() {
        return scene;
    }
}
