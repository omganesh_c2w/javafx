package com.c2w.AllScreens;
import com.google.api.core.ApiFuture;
import com.google.cloud.firestore.*;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class StudentTableView {

    private ObservableList<Person> students = FXCollections.observableArrayList();
    private Firestore db;

    private Stage primaryStage;
    private Scene scene;
    public StudentTableView(Stage primaryStage){
        this.primaryStage = primaryStage;
        this.scene=initScene(primaryStage);
    }
    public Scene getScene(){
        return scene;
    }
    
   public Scene initScene(Stage primaryStage) {
        db = FirebaseConfig.db; // Assuming FirebaseConfig is properly set up

        // TableView setup
        TableView<Person> tableView = new TableView<>();
        tableView.setItems(students);

        TableColumn<Person, Integer> serialNumberColumn = new TableColumn<>("Sr. No");
        serialNumberColumn.setCellValueFactory(new PropertyValueFactory<>("serialNumber"));

        TableColumn<Person, Integer> rollNumberColumn = new TableColumn<>("Roll No");
        rollNumberColumn.setCellValueFactory(new PropertyValueFactory<>("rollNumber"));

        TableColumn<Person, String> firstNameColumn = new TableColumn<>("First Name");
        firstNameColumn.setCellValueFactory(new PropertyValueFactory<>("firstName"));

        TableColumn<Person, String> lastNameColumn = new TableColumn<>("Last Name");
        lastNameColumn.setCellValueFactory(new PropertyValueFactory<>("lastName"));

        tableView.getColumns().addAll(serialNumberColumn, rollNumberColumn, firstNameColumn, lastNameColumn);

        Button  JI_back = new Button("Back");
        JI_back.setAlignment(Pos.BOTTOM_LEFT);
        JI_back.setOnAction(event ->{
            Screen5 screen5=new Screen5(this, primaryStage);
            primaryStage.setScene(screen5.getScreen5Scene());
            primaryStage.show();


        });

        Button studeButton = new Button("Show Students");
        studeButton.setOnMouseClicked(event -> {
            loadStudentsFromFirestore();
        });

        VBox root = new VBox(10);
        root.getChildren().addAll(studeButton, tableView,JI_back);

        Scene scene = new Scene(root, 600, 400);
        // primaryStage.setScene(scene);
        // primaryStage.setTitle("Student Information");
        primaryStage.setFullScreen(true);
        return scene;
    }

   

    private void loadStudentsFromFirestore() {
        students.clear(); // Clear existing data
    
        ExecutorService executor = Executors.newSingleThreadExecutor();
    
        executor.submit(() -> {
            try {
                ApiFuture<QuerySnapshot> future = db.collection("attendance").get();
                QuerySnapshot querySnapshot = future.get();
    
                for (DocumentSnapshot document : querySnapshot.getDocuments()) {
                    Map<String, Object> attendance = document.getData();
                    if (attendance != null) {
                        for (Map.Entry<String, Object> entry : attendance.entrySet()) {
                            String rollNumberStr = entry.getKey();
                            Map<String, Object> studentAttendance = (Map<String, Object>) entry.getValue();
                            String firstName = (String) studentAttendance.get("firstName");
                            String lastName = (String) studentAttendance.get("lastName");
                            boolean isPresent = (boolean) studentAttendance.get("present");
    
                            int rollNumber = Integer.parseInt(rollNumberStr);
    
                            // Create a new Person object
                            Person person = new Person(students.size() + 1, rollNumber, firstName, lastName);
    
                            // Add person to the observable list (and subsequently to the TableView)
                            Platform.runLater(() -> students.add(person));
                        }
                    }
                }
    
                Platform.runLater(() -> System.out.println("Students loaded successfully."));
            } catch (Exception e) {
                Platform.runLater(() -> System.out.println("Error loading students: " + e.getMessage()));
            } finally {
                executor.shutdown();
            }
        });
    }
    

    public static class Person {
        private final int serialNumber;
        private final int rollNumber;
        private final String firstName;
        private final String lastName;

        public Person(int serialNumber, int rollNumber, String firstName, String lastName) {
            this.serialNumber = serialNumber;
            this.rollNumber = rollNumber;
            this.firstName = firstName;
            this.lastName = lastName;
        }

        public int getSerialNumber() {
            return serialNumber;
        }

        public int getRollNumber() {
            return rollNumber;
        }

        public String getFirstName() {
            return firstName;
        }

        public String getLastName() {
            return lastName;
        }
    }
}
