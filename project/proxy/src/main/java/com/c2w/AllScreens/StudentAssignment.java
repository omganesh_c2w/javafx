package com.c2w.AllScreens;




    // package com.c2w.AllScreens;

import com.c2w.Attendance.AttendanceTable;
import com.google.cloud.storage.Acl.Group;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;

public class StudentAssignment  {
    
        
        private StudentloginScreen1 studentloginScreen1 ;
        private Stage prStage;
        private Scene scene;
        private ShowAssignment showAssignment;
        private FirebaseConfig firebaseConfig; 

        public StudentAssignment(StudentloginScreen1 studentloginScreen1, Stage prStage){
            this.prStage=prStage;
            this.studentloginScreen1=studentloginScreen1;
            this.scene=initScene(prStage);
        }
        public StudentAssignment (ShowAssignment showAssignment,Stage prStage){
            this.showAssignment=showAssignment;
            this.prStage=prStage;
            this.scene=initScene(prStage);


        }
        public Scene getScene(){
            return scene;
        }

        public Scene initScene(Stage prStage){
            Font ft=Font.font("Courier New",FontWeight.BOLD,30);
            prStage.setTitle("Student Assi");
            prStage.setHeight(500);
            prStage.setWidth(700);
            prStage.setFullScreen(true); 
    
            Line l=new Line();
            l.setStartX(0);
            l.setStartY(90);
            l.setEndX(2000);
            l.setEndY(90);
            l.setStroke(Color.BLACK); 
            l.setStrokeWidth(3);
     
            HBox hb=new HBox(l);
            hb.setAlignment(Pos.CENTER);
            hb.setPadding(new Insets(100,0,0,0)); //set line padding

            
            // Bottom Buttons 
            Button studymatrialBotton=new Button("Study Material");
            studymatrialBotton.setLayoutX(100);
            studymatrialBotton.setLayoutY(900);
            studymatrialBotton.setFont(ft);
            studymatrialBotton.setStyle("-fx-background-color: #194a7a; -fx-text-fill: #FFFFFF;");
            studymatrialBotton.setOnMouseClicked(event ->{
                System.out.println("Study Material");
                ShowAssignment showassignment=new ShowAssignment(this, prStage);

                prStage.setScene(showassignment.getShowAssigScene());
                prStage.show();
                // prStage.show();
                
            });
    
            DropShadow JI_shadowforStm = new DropShadow();
    
            studymatrialBotton.addEventHandler(MouseEvent.MOUSE_ENTERED, 
        new EventHandler<MouseEvent>() {
             public void handle(MouseEvent e) {
                studymatrialBotton.setEffect(JI_shadowforStm);
            }
    });
    studymatrialBotton.addEventHandler(MouseEvent.MOUSE_EXITED, 
    new EventHandler<MouseEvent>() {
         public void handle(MouseEvent e) {
            studymatrialBotton.setEffect(null);
        }
    });         

    studymatrialBotton.setOnAction(event ->{



    });
            //Assignment Button
            Button assginmentButton=new Button("Assignment");
            assginmentButton.setLayoutX(400);
            assginmentButton.setLayoutY(900);
            assginmentButton.setFont(ft);
           assginmentButton.setStyle("-fx-background-color: #476f95; -fx-text-fill: #FFFFFF;");
           assginmentButton.setOnMouseClicked(event ->{
            

           });
    
            DropShadow JI_shadowforAssi = new DropShadow();
            assginmentButton.addEventHandler(MouseEvent.MOUSE_ENTERED, 
        new EventHandler<MouseEvent>() {
             public void handle(MouseEvent e) {
                assginmentButton.setEffect(JI_shadowforAssi);
            }
    });
    assginmentButton.addEventHandler(MouseEvent.MOUSE_EXITED, 
    new EventHandler<MouseEvent>() {
         public void handle(MouseEvent e) {
            assginmentButton.setEffect(null);
        }
    });

    assginmentButton.setOnAction(event->{
       // ImageShow imgshow = new ImageShow();
    });
           
    Button JI_back5 = new Button("Back");
    JI_back5.setFont(ft);
    JI_back5.setAlignment(Pos.BOTTOM_LEFT);
    JI_back5.setStyle(
    "-fx-background-color: rgba(230, 230, 250, 0.8); " +
    "-fx-background-radius: 30; " + // Rounded corners
    "-fx-border-radius: 30; " + // Border radius
    "-fx-border-color: black; " + // Optional border color
    "-fx-border-width: 2;" // Optional border width
);
 JI_back5.setOnAction(event -> {
    StudentloginScreen1 std=new StudentloginScreen1(this, prStage);
    prStage.setScene(std.getstudentyScene());
    prStage.show();
        
     });
            HBox bottBox=new HBox(30,JI_back5,studymatrialBotton,assginmentButton);
            bottBox.setAlignment(Pos.CENTER);
            bottBox.setPadding(new Insets(40));

        
            

            Pane JI_p2=new Pane();
               JI_p2.getChildren().addAll(l);
               JI_p2.setLayoutX(1000);
              JI_p2.setLayoutY(100);
            
    
              StackPane JI_SP=new StackPane();
              //JI_SP.getChildren().addAll();
              
              
    
          BorderPane root=new BorderPane();
          //  root.setStyle("-fx-background-color:#FEFFD2");
            Image backgroundImage = new Image("/images/bk1.png");
             BackgroundImage background = new BackgroundImage(
              backgroundImage,
              BackgroundRepeat.NO_REPEAT,
              BackgroundRepeat.NO_REPEAT,
              BackgroundPosition.DEFAULT,
              new BackgroundSize(BackgroundSize.AUTO, BackgroundSize.AUTO, false, false, true, true)
              );
              
    
                root.setBackground(new Background(background));
              //  root.getChildren().addAll(JI_Fb,JI_i);
                root.setBottom(bottBox);
              //  root.setTop(hb);
                root.setTop(JI_p2);
                
    
    
        //   Pane JI_p2=new Pane();
        //   JI_p2.getChildren().addAll(JI_Fb,JI_i);
        //   JI_p2.setLayoutX(1000);
        //   JI_p2.setLayoutY(100);
        //   JI_p2.getChildren().addAll(root);

            Scene scene = new Scene(root,500,700);

        //    prStage.setScene(scene);
        //     prStage.show();
        return scene;
        }
    }
    


    

