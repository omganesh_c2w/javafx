package com.c2w.AllScreens;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;

public class Screen1  {

    private Stage primaryStage;
    private Scene scene; // Add this field to store the scene

    public Screen1(Stage primaryStage){
        this.primaryStage=primaryStage;
        this.scene=initScene(primaryStage);
    }

    public void setStage(Scene newScene) {
        primaryStage.setScene(newScene);
    }

    public Scene getScreen1Scene() {
        return this.scene; // This will return the scene stored in the field
    }

    public Scene initScene(Stage prStage){
        Font ft = Font.font("Courier New", FontWeight.BOLD, 50);

        prStage.setTitle("Screen1");
        prStage.setWidth(1000);
        prStage.setHeight(1000);

        Label JI_label = new Label("Create Class");
        JI_label.setAlignment(Pos.TOP_CENTER);
        JI_label.setFont(ft);
        JI_label.setStyle("-fx-text-fill: #B3E5FC");

        Circle JI_cp = new Circle(200);

        // Load the images
        Image bottom = new Image("/images/file.png"); // Ensure the path is correct

        // Create the ImageView objects
        ImageView bottomImageView = new ImageView(bottom);

        // Set the size of the images (optional)
        bottomImageView.setFitWidth(70);
        bottomImageView.setFitHeight(70);

        // Create the buttons with the images
        Button bottomRightButton = new Button();
        bottomRightButton.setStyle("-fx-background-color:#B3E5FC");
        bottomRightButton.setGraphic(bottomImageView);
        bottomRightButton.setShape(JI_cp);

        DropShadow JI_shadowadd = new DropShadow();
        bottomRightButton.addEventHandler(MouseEvent.MOUSE_ENTERED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent e) {
                bottomRightButton.setEffect(JI_shadowadd);
            }
        });

        bottomRightButton.addEventHandler(MouseEvent.MOUSE_EXITED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent e) {
                bottomRightButton.setEffect(null);
            }
        });

        // Action event on bottomRightButton
        bottomRightButton.setOnAction(event -> {
            Screen2 screen2 = new Screen2(this, primaryStage);
            primaryStage.setScene(screen2.getScreen2Scene());
            primaryStage.show();
        });

        // Line line = new Line(); // Create The Horizontal Line On Stage
        // line.setStartX(0); // Starting X Coordinate of Line 
        // line.setStartY(100); // Starting Y Coordinate of Line                  
        // line.setEndX(2000); // End X Coordinate of Line and Width
        // line.setEndY(100); 
        // line.setStrokeWidth(3);

        // Create layouts for top and bottom sections
        VBox topRightBox = new VBox();
        // topRightBox.getChildren().addAll(line);
        topRightBox.setPadding(new Insets(90, 100, 30, 0));

        VBox bottomRightBox = new VBox(bottomRightButton);
        bottomRightBox.setAlignment(Pos.BOTTOM_RIGHT);
        bottomRightBox.setPadding(new Insets(0, 300, 100, 10));

        Pane JI_p = new Pane(JI_label);
        JI_p.setLayoutX(850);

        // Load and add the background image
        Image bkimg = new Image("/images/bk1.png"); // Ensure the path is correct
        ImageView bkimgview = new ImageView(bkimg); 
        bkimgview.setPreserveRatio(true);
        bkimgview.setFitWidth(3000);
        bkimgview.setFitHeight(1200);

        // Create the main layout
        BorderPane mainLayout = new BorderPane();
        mainLayout.getChildren().add(bkimgview); // Add the background image

        mainLayout.setCenter(topRightBox);
        mainLayout.setBottom(bottomRightBox);
        mainLayout.getChildren().addAll(JI_p);
        mainLayout.setPrefSize(400, 300);

        this.primaryStage = prStage;

        // Create the scene and set the main layout
        this.scene = new Scene(mainLayout, 500, 200); // Store the scene in the field
        
        prStage.setScene(this.scene);
        prStage.setFullScreen(true);
        prStage.show();
        return scene;
    }

    
}
