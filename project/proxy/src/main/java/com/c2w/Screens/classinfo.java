package com.c2w.Screens;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;

public class classinfo extends Application {
    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setTitle("Class Information");
        primaryStage.setWidth(800);
        primaryStage.setHeight(600);

        
        Font ft = Font.font("Courier New", FontWeight.BOLD, 20);

        // Creating Labels and TextFields for class information
        Label classNameLabel = new Label("Class Name:");
        classNameLabel.setFont(ft);
        TextField classNameTextField = new TextField();
        classNameTextField.setPrefWidth(600);
        classNameTextField.setFont(ft);

        Label batchLabel = new Label("Batch:");
        batchLabel.setFont(ft);
        TextField batchTextField = new TextField();
        batchTextField.setPrefWidth(600);
        batchTextField.setFont(ft);

        Label subjectLabel = new Label("Subject:");
        subjectLabel.setFont(ft);
        TextField subjectTextField = new TextField();
        subjectTextField.setPrefWidth(600);
        subjectTextField.setFont(ft);

        Label teacherNameLabel = new Label("Teacher Name:");
        teacherNameLabel.setFont(ft);
        TextField teacherNameTextField = new TextField();
        teacherNameTextField.setPrefWidth(600);
        teacherNameTextField.setFont(ft);

        Label codeLabel = new Label("Code:");
        codeLabel.setFont(ft);
        TextField codeTextField = new TextField();
        codeTextField.setPrefWidth(600);
        codeTextField.setFont(ft);

        // Creating GridPane for center alignment
        GridPane gridPane = new GridPane();
        gridPane.setAlignment(Pos.CENTER);
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        gridPane.setPadding(new Insets(20));

        // Labels and TextFields for the GridPane
        gridPane.add(classNameLabel, 0, 0);
        gridPane.add(classNameTextField, 1, 0);
        gridPane.add(batchLabel, 0, 1);
        gridPane.add(batchTextField, 1, 1);
        gridPane.add(subjectLabel, 0, 2);
        gridPane.add(subjectTextField, 1, 2);
        gridPane.add(teacherNameLabel, 0, 3);
        gridPane.add(teacherNameTextField, 1, 3);
        gridPane.add(codeLabel, 0, 4);
        gridPane.add(codeTextField, 1, 4);

        // Create a Button
        Button button = new Button("Submit");
        button.setFont(ft);
        button.setPrefWidth(150);
        button.setPrefHeight(50);

        // Set action event
        button.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                String className = classNameTextField.getText();
                String batch = batchTextField.getText();
                String subject = subjectTextField.getText();
                String teacherName = teacherNameTextField.getText();
                String code = codeTextField.getText();

                System.out.println("Class Name: " + className);
                System.out.println("Batch: " + batch);
                System.out.println("Subject: " + subject);
                System.out.println("Teacher Name: " + teacherName);
                System.out.println("Code: " + code);
            }
        });

        //  Button to the GridPane
        gridPane.add(button, 0, 5, 2, 1);
        GridPane.setHalignment(button, HPos.CENTER); // Center align the button in the grid

        //  background color of the scene
        gridPane.setBackground(new Background(new BackgroundFill(Color.LIGHTBLUE, CornerRadii.EMPTY, Insets.EMPTY)));

        //  Scene with the GridPane as root
        Scene scene = new Scene(gridPane, 600, 400); 
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
