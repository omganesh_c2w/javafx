package com.c2w.Screens;



import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.stage.Stage;

public class homepage extends Application {

   
    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setTitle("PROXY.COM");
        primaryStage.setWidth(500);
        primaryStage.setHeight(700);

        Circle JI_cp = new Circle(200);

        // Load the images
        

        Image bottom = new Image("images/colorkit (4).png");

        // Create the ImageView objects
        ImageView bottomImageView = new ImageView(bottom);

        // Set the size of the images (optional)
        bottomImageView.setFitWidth(70);
        bottomImageView.setFitHeight(70);

        // Create the buttons with the images
        Button bottomRightButton = new Button();
        bottomRightButton.setGraphic(bottomImageView);
        bottomRightButton.setShape(JI_cp);

        DropShadow JI_shadowadd = new DropShadow();
        bottomRightButton.addEventHandler(MouseEvent.MOUSE_ENTERED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent e) {
                bottomRightButton.setEffect(JI_shadowadd);
            }
        });

        bottomRightButton.addEventHandler(MouseEvent.MOUSE_EXITED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent e) {
                bottomRightButton.setEffect(null);
            }
        });





       

        Line line = new Line(0, 250, 1870, 250);

        // Create layouts for top and bottom sections
        VBox topRightBox = new VBox(line);
        topRightBox.setAlignment(Pos.TOP_RIGHT);
        topRightBox.setPadding(new Insets(10, 100, 20, 10));

        VBox bottomRightBox = new VBox(bottomRightButton);
        bottomRightBox.setAlignment(Pos.BOTTOM_RIGHT);
        bottomRightBox.setPadding(new Insets(10, 300, 100, 10));

        // Create the main layout
        BorderPane mainLayout = new BorderPane();

        // Add components to the main layout
        mainLayout.setBottom(bottomRightBox);

        // Load the background image
        Image backgroundImage = new Image("assets/colorkit (4).png");
        BackgroundImage background = new BackgroundImage(
            backgroundImage,
            BackgroundRepeat.NO_REPEAT,
            BackgroundRepeat.NO_REPEAT,
            BackgroundPosition.DEFAULT,
            BackgroundSize.DEFAULT
        );

        // Set the background of the BorderPane
        mainLayout.setBackground(new Background(background));
        



        // Create the scene and set the main layout
        Scene scene = new Scene(mainLayout, 500, 200);
        primaryStage.setScene(scene);
        primaryStage.show();
    // 
    }


}
