package com.c2w.Screens;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;

public class createclass extends Application {
    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setTitle("PROXY.COM");
        primaryStage.setWidth(800);
        primaryStage.setHeight(800);

        // button
        Button centerBottomButton = new Button("Create Class");

        // size of the button
        centerBottomButton.setPrefWidth(300);
        centerBottomButton.setPrefHeight(100);

        // Set custom font
        Font customFont = Font.font("Courier New", FontWeight.BOLD, 30);
        centerBottomButton.setFont(customFont);

        centerBottomButton.setStyle("-fx-background-color:#d1dbe4 ; -fx-text-fill: Grey;");

         DropShadow JI_shadowadd = new DropShadow();
         centerBottomButton.addEventHandler(MouseEvent.MOUSE_ENTERED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent e) {
                centerBottomButton.setEffect(JI_shadowadd);
            }
        });

        centerBottomButton.addEventHandler(MouseEvent.MOUSE_EXITED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent e) {
                centerBottomButton.setEffect(null);
            }
        });

      

        // Load the background image
        Image backgroundImage = new Image("images/colorkit (4).png");
        BackgroundImage background = new BackgroundImage(
            backgroundImage,
            BackgroundRepeat.NO_REPEAT,
            BackgroundRepeat.NO_REPEAT,
            BackgroundPosition.CENTER,
            BackgroundSize.DEFAULT
        );

        BorderPane bp1 = new BorderPane();
        bp1.setBackground(new Background(background));
        bp1.setCenter(centerBottomButton);

        VBox mainLayout = new VBox();
        mainLayout.getChildren().addAll();
        mainLayout.setAlignment(Pos.CENTER);

        // StackPane for positioning the VBox and setting the background
        StackPane stackPane = new StackPane(bp1);
        stackPane.setAlignment(Pos.CENTER);

       
        

       

        // Main layout scene
        Scene scene = new Scene(stackPane, 600, 400);
        primaryStage.setScene(scene);
        primaryStage.show();

        // on click action
        centerBottomButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                System.out.println("Class Information");
            }
        });
    }

   
}
