package com.c2w.StudnetList;

import com.c2w.Attendance.AttendanceTable.Person;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class StudentList extends Application {
      @Override
    public void start(Stage primaryStage) {
        // Create sample data
        ObservableList<Person> data = FXCollections.observableArrayList();

        // Create columns for the TableView
        TableColumn<Person, Integer> serialNumberColumn = new TableColumn<>("Sr. No");
        serialNumberColumn.setCellValueFactory(new PropertyValueFactory<>("serialNumber"));

        TableColumn<Person, Integer> rollNumberColumn = new TableColumn<>("Roll No");
        rollNumberColumn.setCellValueFactory(new PropertyValueFactory<>("rollNumber"));

        TableColumn<Person, String> firstNameColumn = new TableColumn<>("First Name");
        firstNameColumn.setCellValueFactory(new PropertyValueFactory<>("firstName"));

        TableColumn<Person, String> lastNameColumn = new TableColumn<>("Last Name");
        lastNameColumn.setCellValueFactory(new PropertyValueFactory<>("lastName"));

        // Create the TableView and set the data and columns
        TableView<Person> tableView = new TableView<>(data);
        tableView.getColumns().addAll(serialNumberColumn, rollNumberColumn, firstNameColumn, lastNameColumn);

        // Create input fields and button for adding new data
        TextField serialNoInput = new TextField();
        serialNoInput.setPromptText("Sr. No");

        TextField rollNoInput = new TextField();
        rollNoInput.setPromptText("Roll No");

        TextField firstNameInput = new TextField();
        firstNameInput.setPromptText("First Name");

        TextField lastNameInput = new TextField();
        lastNameInput.setPromptText("Last Name");

        Button addButton = new Button("Add");
        addButton.setOnAction((ActionEvent e) -> {
            try {
                int serialNumber = Integer.parseInt(serialNoInput.getText());
                int rollNumber = Integer.parseInt(rollNoInput.getText());
                String firstName = firstNameInput.getText();
                String lastName = lastNameInput.getText();

                Person person = new Person(serialNumber, rollNumber, firstName, lastName);
                data.add(person);

                serialNoInput.clear();
                rollNoInput.clear();
                firstNameInput.clear();
                lastNameInput.clear();
            } catch (NumberFormatException ex) {
                System.out.println("Invalid input. Please enter valid numbers for Serial Number and Roll Number.");
            }
        });

        // Layout for input fields and button
        HBox inputLayout = new HBox(30);
        inputLayout.getChildren().addAll(
                new Label("Sr. No:"), serialNoInput,
                new Label("Roll No:"), rollNoInput,
                new Label("First Name:"), firstNameInput,
                new Label("Last Name:"), lastNameInput,
                addButton);

        VBox layout = new VBox(10);
        layout.getChildren().addAll(tableView, inputLayout);

        // Create the scene and set it on the stage
        Scene scene = new Scene(layout, 600, 400);
        primaryStage.setScene(scene);
        primaryStage.setTitle("TableView Example");
        primaryStage.show();
    }

    public static class Person {
        private final int serialNumber;
        private final int rollNumber;
        private final String firstName;
        private final String lastName;

        public Person(int serialNumber, int rollNumber, String firstName, String lastName) {
            this.serialNumber = serialNumber;
            this.rollNumber = rollNumber;
            this.firstName = firstName;
            this.lastName = lastName;
        }

        public int getSerialNumber() {
            return serialNumber;
        }

        public int getRollNumber() {
            return rollNumber;
        }

        public String getFirstName() {
            return firstName;
        }

        public String getLastName() {
            return lastName;
        }
    }
}


